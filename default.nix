{
	nixpkgs ? import <nixpkgs> {},
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nix-community/naersk/archive/master.tar.gz") {},
}:
with nixpkgs;
{
	ecl = naersk.buildPackage {
		root = pkgs.nix-gitignore.gitignoreSource [
			"*.nix"
			".*"
			"fuzz/"
		] ./.;
		buildInputs = with pkgs; [
			openssl
			pkg-config
		];
		doCheck = true;
		RUST_BACKTRACE = "1";

		RUSTC_BOOTSTRAP = "1"; # The Mozilla nightly overlay results in failing to find openssl at runtime for some reason.
	};

	fuzz = pkgs.runCommand "fuzz" {
		buildInputs = with pkgs; [
			cargo-fuzz
			openssl
			pkg-config
		];
		RUST_BACKTRACE = "1";
	} ''
		echo 'Use nix-shell to run `cargo fuzz run eval`'
		touch $out
	'';
}
