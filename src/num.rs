pub fn int(n: impl Into<num::BigInt>) -> crate::Val { fraction(n, 1) }
pub fn fraction(n: impl Into<num::BigInt>, d: impl Into<num::BigInt>) -> crate::Val {
	get(num::BigRational::new(n.into(), d.into()))
}
pub fn get(n: num::BigRational) -> crate::Val { crate::Val::new_atomic(Num(n)) }

#[derive(PartialEq,PartialOrd)]
struct Num(num::BigRational);

impl crate::Value for Num {
	fn type_str(&self) -> &'static str { "number" }

	fn to_string(&self) -> crate::Val {
		crate::Val::new_atomic(ToString::to_string(&self.0))
	}

	fn get_num(&self) -> Option<&num::BigRational> {
		Some(&self.0)
	}

	fn neg(&self) -> crate::Val {
		get(-&self.0)
	}
}

impl crate::SameOps for Num {
	fn add(&self, that: &Self) -> crate::Val {
		get(&self.0 + &that.0)
	}

	fn subtract(&self, that: &Self) -> crate::Val {
		get(&self.0 - &that.0)
	}

	fn cmp(&self, that: &Self) -> Result<std::cmp::Ordering,crate::Val> {
		self.partial_cmp(that)
			.ok_or_else(||
				crate::Err::new(format!("Failed to compare {:?} and {:?}", *self, *that)))
	}
}

impl std::fmt::Debug for Num {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}", self.0)
	}
}

impl serde::Serialize for Num {
	fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
		if let Some(i) = num::ToPrimitive::to_i64(&self.0) {
			s.serialize_i64(i)
		} else {
			s.serialize_f64(num::ToPrimitive::to_f64(&self.0).unwrap())
		}
	}
}
