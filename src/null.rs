#[derive(Debug)]
pub struct Null;

pub fn get() -> crate::Val {
	crate::WeakVal::Null.upgrade()
}

impl crate::Value for Null {
	fn type_str(&self) -> &'static str { "null" }

	fn to_string(&self) -> crate::Val {
		crate::Val::new_atomic("null".to_owned())
	}

	fn to_bool(&self) -> crate::Val {
		crate::bool::get(false)
	}
}

impl crate::SameOps for Null {
	fn cmp(&self, _that: &Self) -> Result<std::cmp::Ordering,crate::Val> {
		Ok(std::cmp::Ordering::Equal)
	}
}

impl serde::Serialize for Null {
	fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
		s.serialize_unit()
	}
}
