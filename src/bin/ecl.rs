use serde::Serialize;

fn exit_err<T>(e: impl std::error::Error) -> T {
	eprintln!();
	eprintln!("Error serializing: {}", e);
	std::process::exit(1);
}

fn serialize_val_with(val: ecl::Val, output: Output) {
	match output.format {
		Format::Json => {
			let f = if output.compact {
				serde_json::to_writer
			} else {
				serde_json::to_writer_pretty
			};
			f(&mut std::io::stdout(), &val)
				.unwrap_or_else(exit_err);
			println!();
		}
		Format::Lines => {
			val.serialize(
				&mut ecl::lines::Serializer::new(std::io::stdout()))
				.unwrap_or_else(exit_err);
		}
		Format::Yaml => {
			serde_yaml::to_writer(&mut std::io::stdout(), &val)
				.unwrap_or_else(exit_err);
		}
	}
}

#[derive(Copy,Clone,Debug,clap::ValueEnum)]
enum Format {
	Json,
	Lines,
	Yaml,
}

#[derive(Debug,clap::Args)]
struct Output {
	/// Format for robots.
	#[clap(short, long)]
	compact: bool,

	/// Output format.
	#[clap(value_enum, short, long, default_value="json")]
	format: Format,
}

#[derive(Debug,clap::Subcommand)]
enum Command {
	/// Execute from file.
	Load {
		/// ecl file to evaluate.
		file: String,

		/// Call the config with the given arguments.
		#[clap(long, short)]
		args: Option<String>,

		/// Run given expression with the result in scope as _
		#[clap(long, short)]
		select: Option<String>,

		#[clap(flatten)]
		output: Output,
	},
	/// Execute from argument.
	Eval {
		/// ecl string to evaluate.
		code: String,

		#[clap(flatten)]
		output: Output,
	},
}

/// Convert ecl configs into data files.
#[derive(Debug,clap::Parser)]
#[clap(version)]
struct Args {
	#[clap(subcommand)]
	command: Command,
}

fn main() {
	let args: Args = clap::Parser::parse();

	match args.command {
		Command::Load{file, args, select, output} => {
			let mut val = ecl::eval_file(&file);
			if let Some(code) = args {
				let args = ecl::eval("<args>", &code);
				val = val.call(args)
			}
			if let Some(code) = select {
				let select = ecl::hacky_parse_func("--select", "_".to_owned(), &code);
				val = select.call(val);
			}

			match val.eval() {
				Ok(v) => {
					serialize_val_with(v, output);
				}
				Err(e) => {
					eprintln!("{:?}\nError occurred.", e);
					std::process::exit(1)
				}
			}
		},
		Command::Eval{code, output} => {
			let val = ecl::eval("<command-line>", &code);
			serialize_val_with(val, output);
		},
	}
}
