use digest::Digest;
use std::convert::TryInto;
use std::fmt::Write;

static LEAK_COUNT: std::sync::atomic::AtomicUsize = std::sync::atomic::AtomicUsize::new(0);

pub fn internal_reset_leak_count() {
	LEAK_COUNT.store(0, std::sync::atomic::Ordering::SeqCst)
}

struct Leakable;

impl Leakable {
	fn new() -> Self {
		let previous = LEAK_COUNT.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
		assert_ne!(previous, usize::max_value());
		Leakable
	}
}

impl Drop for Leakable {
	fn drop(&mut self) {
		let previous = LEAK_COUNT.fetch_sub(1, std::sync::atomic::Ordering::SeqCst);
		assert_ne!(previous, usize::min_value());
	}
}

static BUILTINS: &[(&str, &(dyn Fn() -> crate::Val + Sync))] = &[
	// nil has to be global 0.
	("nil", &|| crate::nil::get()),

	("cond", &|| new("cond", |v| cond(v))),
	("env", &|| new("env", |name| {
		match std::env::var(name.get_str()?) {
			Ok(val) => crate::Val::new_atomic(val),
			Err(std::env::VarError::NotPresent) => crate::nil::get(),
			Err(e) => e.into(),
		}
	})),
	("error", &|| new("error", |msg|
		crate::Err::new(msg.to_string()?.get_str().unwrap().into()))),
	("error-msg", &|| new_raw("error-msg", |e| {
		if let Ok(e) = e.downcast_ref::<crate::err::Err>() {
			crate::Val::new_atomic(e.msg().to_string())
		} else {
			crate::err::Err::new(format!("Non-error passed to error-msg:{:?}", e))
		}
	})),
	("eval", &|| new("eval", |code| {
		crate::eval("<eval>", code.get_str()?)
	})),
	("hex", &|| new("hex", |v| {
		if let Ok(num) = v.get_num() {
			let mut r = format!("{:x}", num.to_integer());
			if !num.is_integer() {
				write!(r, ".").unwrap();

				let mut n = num.fract();
				let mut last_denom = num.denom().clone();
				loop {
					n *= num::BigInt::from(16);
					write!(r, "{:x}", n.to_integer()).unwrap();

					if n.is_integer() { break }
					if *n.denom() == last_denom {
						n *= num::BigInt::from(16_u32.pow(4));
						write!(r, "{:04x}", n.to_integer()).unwrap();
						break
					}
					last_denom.clone_from(n.denom());
				}
			}
			crate::Val::new_atomic(r)
		} else {
			crate::Err::new(format!("Can't hex:{:?}", v))
		}
	})),
	("index", &|| new("index",
		|i| Builtin::new("index:i", [i],
			|[i], l| {
				l.index(i)
			}))),
	("false", &|| crate::bool::get(false)),
	("filter", &|| new("filter",
		|f| Builtin::new("filter:func", [f],
			|[f], o| o.filter(f)))),
	("flatten", &|| new("flatten", |l| l.flatten())),
	("foldl", &|| new("foldl",
		|f| Builtin::new("foldl:func", [f],
			|[f], accum| Builtin::new("foldl:func:accum", [f, accum],
				|[f, accum], o| o.foldl(f, accum))))),
	("foldr", &|| new("foldr",
		|f| Builtin::new("foldr:func", [f],
			|[f], accum| Builtin::new("foldr:func:accum", [f, accum],
				|[f, accum], o| o.foldr(f, accum))))),
	("load", &|| new("load", |path| {
		crate::eval_file(path.get_str()?)
	})),
	("map", &|| new("map",
		|f| Builtin::new("map:func",
			[f],
			|[f], o| {
				o.map(f)
			}))),
	("null", &|| crate::null::get()),
	("pad", &|| new("pad", |config| {
		let mut with = crate::nil::get();
		let mut to = usize::max_value();

		enum Align { Left, Right, CenterLeft, CenterRight }
		let mut align = Align::Right;

		enum Uneven { Error, Over, Under }
		let mut uneven = Uneven::Error;

		for (k, v) in config.iter_dict()? {
			match k {
				"align" => align = match v.get_str()? {
					"left" => Align::Left,
					"right" => Align::Right,
					"center-left" => Align::CenterLeft,
					"center-right" => Align::CenterRight,
					other => return crate::Err::new(format!(
							"Unknown value for pad:{{align={}}}. Expected left, right center-left or center-right.",
							crate::escape_string(other))),
				},
				"to" => {
					let i = v.get_int()?;
					to = i.try_into()
						.map_err(|_| crate::err::Err::new(format!("pad:{{to={:?}}} is too large.", i)))?;
				}
				"uneven" => uneven = match v.get_str()? {
					"error" => Uneven::Error,
					"over" => Uneven::Over,
					"under" => Uneven::Under,
					other => return crate::Err::new(format!(
							"Unknown value for pad:{{uneven={}}}. Expected error, over or under.",
							crate::escape_string(other))),
				},
				"with" => with = v.to_string()?,
				other => return crate::Err::new(format!(
						"Unknown option for pad:{{{}={:?}}}.",
						crate::format_key(other), v)),
			}
		}

		if to == usize::max_value() {
			return crate::Err::new(format!(
				"Missing required argument pad:{{to=<num>}}."))
		}

		Builtin::new("pad:config", [with], move |data, val| {
			let val = val.to_string()?;
			let s = val.get_str()?;

			if s.len() >= to {
				return val
			}

			let missing = to - s.len();

			let with = if data[0].is_nil().unwrap() {
				" "
			} else {
				data[0].get_str().unwrap()
			};

			let partial = missing % with.len();
			let mut full = missing / with.len();

			if partial != 0 {
				match uneven {
					Uneven::Error => return crate::Err::new(format!(
						"Cannot pad string, no multiple of length {} padding will fill the required {} chars.",
						with.len(), partial)),
					Uneven::Under => {}
					Uneven::Over => full += 1,
				}
			}

			let (left, right) = match align {
				Align::Left => (0, full),
				Align::Right => (full, 0),
				Align::CenterLeft => (full/2, full/2 + full%2),
				Align::CenterRight => (full/2 + full%2, full/2),
			};

			let mut buf = String::with_capacity(s.as_bytes().len() + with.as_bytes().len() * full);
			for _ in 0..left { buf.push_str(with) }
			buf.push_str(s);
			for _ in 0..right { buf.push_str(with) }

			crate::Val::new_atomic(buf)
		})
	})),
	("panic", &|| new("panic", |msg| {
		if cfg!(fuzzing) {
			return crate::err::Err::new(format!(
				"builtin panic {:?}, not actually panicing because this ecl was built in fuzzing mode.",
				msg));
		}

		panic!("Script called panic: {:?}", msg)
	})),
	("read", &|| new("read", |path| {
		let content = crate::fs::read_to_string(path.get_str()?)?;
		crate::Val::new_atomic(content)
	})),
	("replace", &|| new("replace", |config| {
		let mut regex = None;
		let mut text = None;

		let mut case = true;
		let mut multiline = true;
		let mut multiline_dot = None;

		let mut limit: usize = 0;

		let mut with = None;

		for (k, v) in config.iter_dict()? {
			match k {
				"case" => case = v.to_bool()?,
				"limit" => {
					limit = v.get_int()?
						.try_into()
						.map_err(|_: std::num::TryFromIntError|
							crate::err::Err::new(format!(
								"Invalid value for limit {}", limit)))?;
				}
				"multiline" => multiline = v.to_bool()?,
				"multiline-dot" => {
					if v.is_nil()? {
						multiline_dot = None;
					} else {
						multiline_dot = Some(v.to_bool()?);
					}
				}
				"regex" => {
					regex = Some(v);
				}
				"text" => {
					text = Some(v);
				}
				"with" => {
					with = Some(v.to_string()?);
				}
				other => return crate::Err::new(format!(
						"Unknown option for replace:{{{}={:?}}}.",
						crate::format_key(other), v)),
			}
		}

		let text = text.map(|v| v.get_str().map(regex::escape));

		let regex = match (&regex, &text) {
			(Some(v), None) => v.get_str()?,
			(None, Some(v)) => v.as_ref().map_err(|v| v.clone())?,
			(Some(_), Some(_)) => return crate::err::Err::new(format!(
				"Only one of regex and text can be passed to replace.")),
			(None, None) => return crate::err::Err::new(format!(
				"Missing required argument regex or text.")),
		};

		let mut builder = regex::RegexBuilder::new(regex);
		builder.case_insensitive(!case);
		builder.multi_line(!multiline);
		builder.dot_matches_new_line(multiline_dot.unwrap_or(multiline));
		let regex = match builder.build() {
			Ok(regex) => regex,
			Err(e) => return crate::err::Err::new(e.to_string()),
		};

		let with = with.unwrap_or_else(|| crate::Val::new_atomic(String::new()));

		Builtin::new("replace:config", [with], move |[with], text| {
			match regex.replacen(text.get_str()?, limit, with.get_str()?) {
				std::borrow::Cow::Borrowed(s) => {
					// No replacements made.
					debug_assert_eq!(s, text.get_str().unwrap());
					text
				}
				std::borrow::Cow::Owned(s) => {
					crate::Val::new_atomic(s)
				}
			}
		})
	})),
	("reverse", &|| new("reverse", |v| v.reverse())),
	("sha1", &|| new("sha1", |v| {
		let s = v.get_str()?;
		let array = sha1::Sha1::digest(s.as_bytes());
		let bytes = array.as_slice();
		let int = num::BigUint::from_bytes_be(bytes);
		crate::num::int(int)
	})),
	("slice", &|| new("slice", |config| {
		let mut count: Option<i64> = None;
		let mut from: i64 = 0;
		let mut to: Option<i64> = None;

		for (k, v) in config.iter_dict()? {
			match k {
				"count" => {
					count = if v.is_nil()? {
						None
					} else {
						Some(v.get_int()?.try_into()?)
					}
				}
				"from" => {
					from = v.get_int()?.try_into()?;
				}
				"to" => {
					to = if v.is_nil()? {
						None
					} else {
						Some(v.get_int()?.try_into()?)
					}
				}
				other => return crate::Err::new(format!(
						"Unknown option for slice:{{{}={:?}}}.",
						crate::format_key(other), v)),
			}
		}

		new("slice:config", move |val| {
			if let Ok(list) = val.downcast_ref::<crate::list::List>() {
				let len: i64 = crate::val::Value::len(list).unwrap().try_into()?;

				if from > len || -from > len {
					return crate::Err::new(format!(
						"slice:{{from={}}} is longer than {} elements in {:?}",
						from, len, list))
				}

				let from = if from < 0 {
					len + from
				} else {
					from
				};

				let to = match (count, to) {
					(Some(count), None) => {
						let to = from + count;
						if to > len {
							return crate::Err::new(format!(
								"slice:{{from={} count={}}} is longer than {} elements in {:?}",
								from, count, len, list))
						}
						to
					}
					(None, Some(to)) => {
						if to > len || -to > len {
							return crate::Err::new(format!(
								"slice:{{to={}}} is longer than {} elements in {:?}",
								to, len, list))
						}

						if to < 0 {
							len + to
						} else {
							to
						}
					}
					(None, None) => len,
					(Some(count), Some(to)) => return crate::err::Err::new(format!(
						"Conflicting arguments slice:{{count={} to={}}}",
						count, to)),
				};

				list.slice(from.try_into()?, to.try_into()?)
			} else {
				crate::err::Err::new(format!(
					"Can't slice {:?}", val))
			}
		})
	})),
	("split", &|| new("split", |config| {
		let mut regex = None;
		let mut text = None;

		let mut case = true;
		let mut multiline = true;
		let mut multiline_dot = None;

		let mut limit = usize::max_value();

		for (k, v) in config.iter_dict()? {
			match k {
				"case" => case = v.to_bool()?,
				"limit" => {
					limit = v.get_int()?
						.try_into()
						.map_err(|_: std::num::TryFromIntError|
							crate::err::Err::new(format!(
								"Invalid value for limit {}", limit)))?;
					if limit == 0 {
						limit = usize::max_value();
					}
				}
				"multiline" => multiline = v.to_bool()?,
				"multiline-dot" => {
					if v.is_nil()? {
						multiline_dot = None;
					} else {
						multiline_dot = Some(v.to_bool()?);
					}
				}
				"regex" => {
					regex = Some(v);
				}
				"text" => {
					text = Some(v);
				}
				other => return crate::Err::new(format!(
						"Unknown option for replace:{{{}={:?}}}.",
						crate::format_key(other), v)),
			}
		}

		let text = text.map(|v| v.get_str().map(regex::escape));

		let regex = match (&regex, &text) {
			(Some(v), None) => v.get_str()?,
			(None, Some(v)) => v.as_ref().map_err(|v| v.clone())?,
			(Some(_), Some(_)) => return crate::err::Err::new(format!(
				"Only one of regex and text can be passed to replace.")),
			(None, None) => "\\s+",
		};

		let empty_pattern = regex.is_empty();
		if empty_pattern && limit != usize::max_value() {
			limit += 1;
		}

		let mut builder = regex::RegexBuilder::new(regex);
		builder.case_insensitive(!case);
		builder.multi_line(!multiline);
		builder.dot_matches_new_line(multiline_dot.unwrap_or(multiline));
		let regex = match builder.build() {
			Ok(regex) => regex,
			Err(e) => return crate::err::Err::new(e.to_string()),
		};

		new("split:config", move |text| {
			let pool = crate::mem::Pool::new();

			let vals = regex.splitn(text.get_str()?, limit)
				.skip(if empty_pattern { 1 } else { 0 })
				.map(|chunk| {
					let val = crate::Val::new_atomic(chunk.to_string());
					crate::thunk::shim(val.downgrade(&pool))
				})
				.collect();

			crate::list::List::of_vals(pool, vals)
		})
	})),
	("to-json", &|| new("to-json", |data| {
		let s = serde_json::to_string(&data)?;
		crate::Val::new_atomic(s)
	})),
	("to-yaml", &|| new("to-yaml", |data| {
		let s = serde_yaml::to_string(&data)?;
		crate::Val::new_atomic(s)
	})),
	("true", &|| crate::bool::get(true)),
	("type", &|| new_raw("type", |v| crate::Val::new_atomic(v.type_str().to_owned()))),
	("_testing_assert_cache_eval", &|| {
		let unevaluated = std::cell::Cell::new(true);
		let func = move |r| {
			assert!(unevaluated.get(), "Called twice");
			unevaluated.set(false);
			r
		};
		new_raw("_testing_assert_cache_eval", func)
	}),
	("_testing_leak_count", &|| {
		crate::num::int(LEAK_COUNT.load(std::sync::atomic::Ordering::SeqCst))
	}),
	("_testing_leak_trace", &|| {
		let tracker = Leakable::new();
		let func = move |r| {
			let _ = tracker;
			r
		};
		new_raw("_testing_leak_trace", func)
	}),
];

pub fn builtin_id(key: &str) -> Option<usize> {
	BUILTINS.iter().position(|p| p.0 == key)
}

pub fn get_id(id: usize) -> crate::Val {
	BUILTINS[id].1()
}

pub struct Builtin<F, const D: usize>{
	name: &'static str,
	func: F,
	pool: crate::mem::PoolRef,
	data: [crate::WeakVal; D],
}

fn new<F: Fn(crate::Val) -> crate::Val + 'static>(name: &'static str, func: F) -> crate::Val {
	Builtin::new(name, [], move |_, a| func(a?))
}

fn new_raw<F: Fn(crate::Val) -> crate::Val + 'static>(name: &'static str, func: F) -> crate::Val {
	Builtin::new(name, [], move |_, a| func(a))
}

impl<
	F: 'static + Fn([crate::Val; D], crate::Val) -> crate::Val,
	const D: usize
> Builtin<F, D> {
	fn new(name: &'static str, d: [crate::Val; D], func: F) -> crate::Val {
		let pool = crate::mem::Pool::new();
		let data = d.map(|v| v.clone().downgrade(&pool));
		crate::Val::new(Builtin{
			name,
			func,
			pool: pool.downgrade(),
			data,
		}, pool)
	}
}

impl<
	F,
	const D: usize,
> crate::SameOps for Builtin<F, D> {
	fn eq(&self, that: &Self) -> crate::Val {
		crate::bool::get(std::ptr::eq(self, that))
	}
}

impl<
	F: 'static + Fn([crate::Val; D], crate::Val) -> crate::Val,
	const D: usize
> crate::Value for Builtin<F, D> {
	fn pool(&self) -> Option<&crate::mem::PoolRef> {
		Some(&self.pool)
	}

	fn call(&self, arg: crate::Val) -> crate::Val {
		let data = self.data.each_ref().map(|v| v.upgrade());
		let v = (self.func)(data, arg)
			.annotate_with(|| format!("Calling builtin {}.", self.name));
		v
	}

	fn type_str(&self) -> &'static str { "function" }
}

impl<
	F,
	const D: usize
> std::fmt::Debug for Builtin<F, D> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "<builtin {:?}>", self.name)
	}
}

impl<F, const D: usize> serde::Serialize for Builtin<F, D> {
	fn serialize<S: serde::Serializer>(&self, _: S) -> Result<S::Ok, S::Error> {
		Err(serde::ser::Error::custom(format!("builtin {} can't be serialized", self.name)))
	}
}

fn cond(args: crate::Val) -> crate::Val {
	args.downcast_ref::<crate::list::List>()?;

	let mut current = 0;
	let len = args.len().unwrap();
	loop {
		if current == len { return crate::nil::get() }
		if current + 1 == len { return args.index_int(current) }
		if args.index_int(current).to_bool()? {
			return args.index_int(current + 1)
		}
		current += 2;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	fn get(key: &str) -> crate::Val {
		BUILTINS.iter()
			.find(|p| p.0 == key)
			.map(|p| p.1())
			.unwrap_or_else(|| crate::Err::new(format!("No global {:?}", key)))
	}

	#[test]
	#[should_panic(expected="Baby's first error")]
	fn panic() {
		let v = crate::eval("<str>", r###"
			{
				msg = "Baby's first" + " error"
				boom = panic:msg
			}.boom
		"###);
		println!("Returned value: {:?}", v);
	}

	#[test]
	fn assert_once_once() {
		let v = get("_testing_assert_cache_eval");
		assert_eq!(v.call(crate::num::int(6)).get_int().unwrap(), 6);
	}

	#[test]
	#[should_panic(expected="Called twice")]
	fn assert_once_twice() {
		let v = get("_testing_assert_cache_eval");
		assert_eq!(v.call(crate::num::int(4)).get_int().unwrap(), 4);
		assert_eq!(v.call(crate::num::int(5)).get_int().unwrap(), 5);
	}
}
