lazy_static::lazy_static! {
	static ref CLIENT: reqwest::blocking::Client = reqwest::blocking::Client::builder()
		.user_agent(concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION")))
		.connect_timeout(std::time::Duration::from_secs(10))
		.timeout(std::time::Duration::from_secs(60))
		.pool_idle_timeout(None)
		.build()
		.unwrap();
}

fn scheme(url: &str) -> Option<&str> {
	if !url.chars().nth(0).unwrap().is_alphabetic() { return None }

	let i = url.find(|c: char| !c.is_alphanumeric() && !"+.-".contains(c))?;

	if &url[i..=i] == ":" {
		Some(&url[..i])
	} else {
		None
	}
}

pub fn read(path: &str) -> Result<Vec<u8>, crate::Val> {
	Ok(match scheme(path) {
		Some("file") | None => {
			let path = path.strip_prefix("file:").unwrap_or(path);

			std::fs::read(path)
				.map_err(|e| crate::err::Err::new(format!("Failed to read {:?}: {:?}", path, e)))?
		}
		Some("http") | Some("https") => {
			let mut res = CLIENT.get(path).send()?;
			let mut buf = Vec::with_capacity(4096);
			res.copy_to(&mut buf)?;
			buf
		}
		Some(other) => return Err(crate::err::Err::new(format!("Unknown scheme opening {:?}. If this is a path consider prefixing with \"file:\" or \"./\"", other))),
	})
}

pub fn read_to_string(path: &str) -> Result<String, crate::Val> {
	let buf = read(path)?;
	String::from_utf8(buf)
		.map_err(|e| crate::err::Err::new(format!("Invalid UTF-8 in {:?}: {}", path, e)))
}

#[test]
fn test_load_http() {
	let server = httptest::Server::run();

	server.expect(
		httptest::Expectation::matching(httptest::matchers::request::method_path("GET", "/one.ecl"))
			.respond_with(
				httptest::responders::status_code(200).body("value = (load:./two.ecl).other-value")));
	server.expect(
		httptest::Expectation::matching(httptest::matchers::request::method_path("GET", "/two.ecl"))
			.respond_with(
				httptest::responders::status_code(200).body("other-value = 1")));

	let code = format!("((load:\"{:?}\").value)", server.url("/one.ecl"));
	dbg!(&code);
	assert_eq!(crate::eval("<string>", &code).get_int().unwrap(), 1);
}
