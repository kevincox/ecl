pub fn get(b: bool) -> crate::Val { crate::WeakVal::Bool(b).upgrade() }

impl crate::Value for bool {
	fn type_str(&self) -> &'static str { "bool" }

	fn to_string(&self) -> crate::Val {
		crate::Val::new_atomic(ToString::to_string(self))
	}

	fn to_bool(&self) -> crate::Val {
		get(*self)
	}

	fn get_bool(&self) -> Option<bool> {
		Some(*self)
	}
}

impl crate::SameOps for bool {
	fn cmp(&self, that: &Self) -> Result<std::cmp::Ordering,crate::Val> {
		Ok(std::cmp::Ord::cmp(self, that))
	}
}
