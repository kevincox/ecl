#[derive(Debug)]
enum PoolData {
	Dead,
	Merged(PoolRef),
	Alive{
		refcount: usize,
		values: Vec<std::rc::Rc<dyn crate::Value>>,
	},
}

#[derive(Clone,Debug)]
pub struct PoolRef(std::rc::Rc<std::cell::RefCell<PoolData>>);

impl PoolRef {
	fn dec_ref(&self) {
		let root = self.get_root();
		let mut data = root.0.borrow_mut();
		let refcount = match &mut *data {
			PoolData::Alive{refcount, ..} => refcount,
			other => unreachable!("Decrementing ref on dead Pool {:?}", other),
		};

		if *refcount > 1 {
			*refcount -= 1;
		} else {
			*data = PoolData::Dead;
		}
	}

	fn inc_ref(&self) {
		let root = self.get_root();
		let mut data = root.0.borrow_mut();
		match &mut*data {
			PoolData::Alive{refcount, ..} => {
				*refcount += 1
			}
			other => unreachable!("Incrementing ref on dead Pool {:?}", other),
		}
	}

	pub fn upgrade(self) -> Pool {
		self.inc_ref();
		Pool(self)
	}

	fn get_root(&self) -> PoolRef {
		let mut this = self.0.borrow_mut();
		if let PoolData::Merged(ref mut pool) = &mut*this {
			let root = pool.get_root();
			*pool = root.clone();
			return root
		}
		self.clone()
	}

	pub fn push(&self, v: std::rc::Rc<dyn crate::Value>) {
		match *self.get_root().0.borrow_mut() {
			PoolData::Alive{ref mut values, ..} => {
				values.push(v);
			},
			_ => unreachable!(),
		}
	}

	pub fn merge(&self, that: &PoolRef) {
		let this = self.get_root();
		let that = that.get_root();

		if std::ptr::eq(&*this.0, &*that.0) {
			return
		}

		let mut this_data = this.0.borrow_mut();
		let mut that_data = that.0.borrow_mut();

		match (&mut*this_data, &mut*that_data) {
			(
				PoolData::Alive{values: this_values, refcount: this_refcount},
				PoolData::Alive{values: that_values, refcount: that_refcount})
			=> {
				*this_refcount += *that_refcount;
				this_values.extend(that_values.drain(..));
			},
			_ => unreachable!(),
		}


		std::mem::drop(this_data);
		*that_data = PoolData::Merged(this);
	}
}

#[derive(Debug)]
pub struct Pool(PoolRef);

impl Pool {
	pub fn new() -> Self {
		Pool(
			PoolRef(
				std::rc::Rc::new(
					std::cell::RefCell::new(
						PoolData::Alive{
							refcount: 1,
							values: Vec::new(),
						}))))
	}

	pub fn downgrade(&self) -> PoolRef {
		self.0.clone()
	}
}

impl std::ops::Deref for Pool {
	type Target = PoolRef;

	fn deref(&self) -> &PoolRef {
		&self.0
	}
}

impl Clone for Pool {
	fn clone(&self) -> Self {
		self.0.inc_ref();
		Pool(self.0.clone())
	}
}

impl Drop for Pool {
	fn drop(&mut self) {
		self.0.dec_ref();
	}
}
