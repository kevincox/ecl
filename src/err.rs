use std;

#[derive(Clone)]
pub struct Err {
	msg: String,
	loc: Loc,
	chained: crate::Val,
}

impl Err {
	pub fn new(msg: String) -> crate::Val {
		Self::new_at(Default::default(), msg)
	}

	pub fn new_at(loc: Loc, msg: String) -> crate::Val {
		crate::Val::new_atomic(Err{msg, loc, chained: crate::nil::get()})
	}

	pub fn new_from(chained: crate::Val, msg: String) -> crate::Val {
		Self::new_from_at(chained, Default::default(), msg)
	}

	pub fn new_from_at(chained: crate::Val, loc: Loc, msg: String) -> crate::Val {
		debug_assert!(chained.is_err(), "Expected to chain to error got {:#?}", chained);
		crate::Val::new_atomic(Err{msg, loc, chained})
	}

	fn from(e: &dyn std::error::Error) -> crate::Val {
		if let Some(sub) = e.source() {
			Self::from(sub).annotate(&format!("{}", e))
		} else {
			Err::new(format!("{}", e))
		}
	}

	pub fn msg(&self) -> &str {
		if self.chained.is_nil().unwrap_or(false) {
			&self.msg
		} else {
			self.chained.downcast_ref::<crate::err::Err>().unwrap().msg()
		}
	}
}

impl crate::Value for Err {
	fn type_str(&self) -> &'static str { "error" }
	fn is_err(&self) -> bool { true }

	fn eval(&self) -> Result<(),crate::Val> {
		unreachable!()
	}
}

impl crate::SameOps for Err {
}

impl std::fmt::Debug for Err {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		if !self.loc.file.is_nil().unwrap_or(false) {
			write!(f, "{} ", self.loc)?;
		}
		write!(f, "{}", self.msg)?;
		if self.chained.is_err() {
			writeln!(f)?;
			write!(f, "{:?}", self.chained)?;
		}
		Ok(())
	}
}

impl serde::Serialize for Err {
	fn serialize<S: serde::Serializer>(&self, _: S) -> Result<S::Ok, S::Error> {
		Err(serde::ser::Error::custom("errors can't be serialized"))
	}
}

impl std::convert::From<crate::grammar::ParseError> for crate::Val {
	fn from(e: crate::grammar::ParseError) -> Self {
		Err::from(&e)
	}
}

impl std::convert::From<reqwest::Error> for crate::Val {
	fn from(e: reqwest::Error) -> Self {
		Err::from(&e)
	}
}

impl std::convert::From<serde_json::Error> for crate::Val {
	fn from(e: serde_json::Error) -> Self {
		Err::from(&e)
	}
}

impl std::convert::From<serde_yaml::Error> for crate::Val {
	fn from(e: serde_yaml::Error) -> Self {
		Err::from(&e)
	}
}

impl std::convert::From<std::convert::Infallible> for crate::Val {
	fn from(_: std::convert::Infallible) -> Self {
		unreachable!()
	}
}

impl std::convert::From<std::env::VarError> for crate::Val {
	fn from(e: std::env::VarError) -> Self {
		Err::from(&e)
	}
}

impl std::convert::From<std::io::Error> for crate::Val {
	fn from(e: std::io::Error) -> Self {
		Err::from(&e)
	}
}

impl std::convert::From<std::num::TryFromIntError> for crate::Val {
	fn from(e: std::num::TryFromIntError) -> Self {
		Err::from(&e)
	}
}

#[derive(Clone)]
pub struct Loc {
	pub file: crate::Val,
	pub line: usize,
	pub col: usize,
}

impl Default for Loc {
	fn default() -> Self {
		Loc {
			file: crate::nil::get(),
			line: 0,
			col: 0,
		}
	}
}

impl std::fmt::Display for Loc {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self.file.to_string().get_str() {
			Ok(file) => {
				if self.col != 0 {
					write!(f, "{}:{}.{}", file, self.line, self.col)
				} else {
					write!(f, "{}:unknown", file)
				}
			}
			Err(err) => {
				eprintln!("Error getting file name: {:?}", err);
				if self.col != 0 {
					write!(f, "<err>:{}.{}", self.line, self.col)
				} else {
					write!(f, "Unknown location")
				}
			}
		}
	}
}
