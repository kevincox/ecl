use std;

type ParseResult = Result<crate::ast::Almost,ParseError>;

#[derive(Clone,Copy,PartialEq)]
pub struct Loc {
	pub line: usize,
	pub col: usize,
}

impl std::fmt::Debug for Loc {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}.{}", self.line, self.col)
	}
}

#[derive(Clone,Debug,PartialEq)]
pub enum StrType {
	String,
	Relative,
	Parent,
}

#[derive(Clone,Debug,PartialEq)]
pub enum Token {
	Add,
	And,
	Assign,
	Call,
	DictClose,
	DictOpen,
	Dot,
	Eq,
	ExponentTooLarge,
	Func,
	Great,
	GreatEq,
	Ident(String),
	Less,
	LessEq,
	ListClose,
	ListOpen,
	Ne,
	Neg,
	Not,
	Num(num::BigRational),
	Or,
	ParenClose,
	ParenOpen,
	Percent,
	Slash,
	Star,
	StrChunk(String),
	StrClose,
	StrOpen(StrType),
	StructIdent(usize, String),
	Unexpected(char),
	Unfinished,
}

#[derive(Debug,PartialEq)]
pub enum ErrorType {
	Unexpected(Token),
	Unfinished,
	Unused(Token),
}

impl std::fmt::Display for ErrorType {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			ErrorType::Unexpected(token) =>
				write!(f, "Unexpected {:?}", token),
			ErrorType::Unfinished =>
				write!(f, "Unexpected end of file"),
			ErrorType::Unused(token) =>
				write!(f, "Unused {:?} after end of document.", token),
		}
	}
}

#[derive(Debug,PartialEq)]
pub struct ParseError {
	pub typ: ErrorType,
	pub msg: &'static str,
	pub loc: Loc,
}

impl ParseError {
	fn into_val(self, file: impl Into<String>) -> crate::Val {
		let loc = crate::err::Loc {
			file: crate::Val::new_atomic(file.into()),
			line: self.loc.line,
			col: self.loc.col,
		};
		crate::err::Err::new_at(loc, format!("Failed to parse {}: {}", self.msg, self.typ))
	}
}

impl std::fmt::Display for ParseError {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{} {:?} at {:?}", self.msg, self.typ, self.loc)
	}
}

impl std::error::Error for ParseError { }

macro_rules! while_next {
	{$i:ident, $($($p:pat)|+ => $e:expr),+ $(,)* } => {
		loop {
			match $i.next() {
				$($(Some($p))|+ => $e),+
				Some(c) => { $i.unget(c); break },
				None => break,
			}
		}
	}
}

macro_rules! if_next {
	{$i:ident, $($($p:pat)|+ => $e:expr),+ $(,)* } => {
		match $i.next() {
			$($(Some($p))|+ => { Some($e) }),+
			Some(c) => { $i.unget(c); None },
			None => None,
		}
	}
}

#[derive(Debug,PartialEq)]
enum LexerMode {
	Code(usize),
	String{depth: usize, indent: String},
	StringEnd,
	Path,
}

struct Lexer<Input: Iterator<Item=char>> {
	input: Input,
	current: Option<char>,
	state: Vec<LexerMode>,
	start_loc: Loc,
	loc: Loc,
}

impl<Input: Iterator<Item=char>> Lexer<Input> {
	fn new(input: Input) -> Self {
		let mut statestack = Vec::with_capacity(2);
		statestack.push(LexerMode::Code(2));

		Lexer {
			input: input,
			current: None,
			state: statestack,
			start_loc: Loc{line: 1, col: 0},
			loc: Loc{line: 1, col: 0},
		}
	}

	fn ident_start_char(c: char) -> bool{
		c.is_alphabetic() || c == '_'
	}

	fn ident_char(c: char) -> bool{
		c.is_alphanumeric() || c == '-' || c == '_'
	}

	fn ident_str(&mut self, first: char) -> Result<String,Token> {
		if !Self::ident_start_char(first) { return Err(self.unexpected(first)) }

		let mut s = String::new();
		s.push(first);

		while let Some(c) = self.next() {
			if !Self::ident_char(c) {
				self.unget(c);
				break
			}
			s.push(c);
		}

		Ok(s)
	}

	fn ident(&mut self, first: char) -> Token {
		self.ident_str(first).map(Token::Ident).unwrap_or_else(|t| t)
	}

	fn next_is_ident(&mut self) -> bool {
		if let Some(c) = self.peek() {
			return Self::ident_char(c)
		}
		false
	}

	fn raw_next(&mut self) -> Option<char> {
		let next = self.input.next();
		next
	}

	fn unexpected(&mut self, c: char) -> Token {
		self.start_loc = self.loc; // Report the bad char, not the start of the token.
		Token::Unexpected(c)
	}

	fn next(&mut self) -> Option<char> {
		let next = self.current.take().or_else(|| self.raw_next());
		if next == Some('\n') {
			self.loc.line += 1;
			self.loc.col = 0;
		} else {
			self.loc.col += 1;
		}
		// println!("At {:?} got {:?}", self.loc, next);
		next
	}

	fn peek(&mut self) -> Option<char> {
		if self.current.is_none() {
			self.current = self.raw_next();
		}
		self.current
	}

	fn consume(&mut self, c: char) -> bool {
		if self.peek() == Some(c) { self.next(); true } else { false }
	}

	fn unget(&mut self, c: char) {
		debug_assert!(self.current.is_none());
		if c == '\n' { self.loc.line -= 1 } else { self.loc.col -= 1 }
		self.current = Some(c);
	}

	fn next_token(&mut self) -> Option<Token> {
		// Capture at start.
		self.start_loc = self.loc;
		self.start_loc.col += 1;

		match self.state.last().unwrap() {
			&LexerMode::Code(_) => self.lex_code(),
			&LexerMode::Path => Some(self.lex_path()),
			&LexerMode::String{..} => Some(self.lex_str()),
			&LexerMode::StringEnd => {
				self.state.pop();
				Some(Token::StrClose)
			}
		}
	}

	// Parses the bit of a number after the 0.
	fn num(&mut self) -> Token {
		if_next!(self,
			'b' => self.num_base(2, '0'),
			'o' => self.num_base(8, '0'),
			'd' => self.num_base(10, '0'),
			'x' => self.num_base(16, '0'),
			c@'0'..='9' => self.num_base(10, c),
			'.' => { self.unget('.'); self.num_base(10, '0') }
		).unwrap_or(Token::Num(num::BigRational::from_integer(0.into())))
	}

	fn num_int(&mut self, base: u32, first: char) -> num::BigInt {
		let n = first.to_digit(base).unwrap();
		let mut n = num::BigInt::from(n);

		while_next! { self,
			'_' => {},
			c@'0'..='9' |
			c@'a'..='f' |
			c@'A'..='F' => {
				if let Some(d) = c.to_digit(base) {
					n *= base as u64;
					n += d as u64;
				} else {
					self.unget(c);
					break;
				}
			},
		}

		n
	}

	fn num_base(&mut self, base: u32, first: char) -> Token {
		let mut numerator = self.num_int(base, first);
		let mut denominator = num::BigInt::from(1);

		if self.consume('.') {
			while_next!{self,
				'_' => {},
				c@'0'..='9' |
				c@'a'..='f' |
				c@'A'..='F' => {
					if let Some(d) = c.to_digit(base) {
						numerator *= base;
						numerator += d;
						denominator *= base;
					} else {
						self.unget(c);
						break
					}
				},
			}
		}

		let bindec = |this: &mut Self, bin: u128, dec| -> num::BigInt {
			if this.consume('i') { bin } else { dec }.into()
		};

		match self.next() {
			Some('E') => numerator *= bindec(self, 2u128.pow(60), 10u128.pow(18)),
			Some('P') => numerator *= bindec(self, 2u128.pow(50), 10u128.pow(15)),
			Some('T') => numerator *= bindec(self, 2u128.pow(40), 10u128.pow(12)),
			Some('G') => numerator *= bindec(self, 2u128.pow(30), 10u128.pow(9)),
			Some('M') => numerator *= bindec(self, 2u128.pow(20), 10u128.pow(6)),
			Some('K') => numerator *= bindec(self, 2u128.pow(10), 10u128.pow(3)), // Accept this as kilo is the only >1 lower case.
			Some('k') => numerator *= bindec(self, 2u128.pow(10), 10u128.pow(3)),
			Some('m') => denominator *= bindec(self, 2u128.pow(10), 10u128.pow(3)),
			Some('u') => denominator *= bindec(self, 2u128.pow(20), 10u128.pow(6)),
			Some('µ') => denominator *= bindec(self, 2u128.pow(30), 10u128.pow(9)),
			Some('n') => denominator *= bindec(self, 2u128.pow(40), 10u128.pow(12)),
			Some('p') => denominator *= bindec(self, 2u128.pow(50), 10u128.pow(15)),
			Some('f') => denominator *= bindec(self, 2u128.pow(60), 10u128.pow(18)),
			Some('a') => denominator *= bindec(self, 2u128.pow(70), 10u128.pow(21)),
			Some('e') => {
				if base != 10 {
					return self.unexpected('e')
				}

				let positive = match self.next() {
					Some('+') => true,
					Some('-') => false,
					Some(c) => { self.unget(c); true },
					None => true,
				};

				let exp = self.num_int(10, '0');
				let exp = match num::ToPrimitive::to_u32(&exp) {
					Some(e) => e,
					None => return Token::ExponentTooLarge,
				};
				if cfg!(fuzzing) && exp > 100 {
					return Token::ExponentTooLarge
				}
				let factor = num::BigInt::from(base).pow(exp);
				if positive {
					numerator *= factor;
				} else {
					denominator *= factor;
				}
			}
			Some(c) => self.unget(c),
			None => {}
		}

		if self.next_is_ident() {
			let c = self.next().unwrap();
			return self.unexpected(c)
		}

		Token::Num(num::BigRational::new(numerator, denominator))
	}

	fn relative_reference(&mut self) -> Token {
		let mut up = 1;
		loop {
			match self.next() {
				Some('.') => up += 1,
				Some(c) =>
					return self.ident_str(c)
						.map(|i| Token::StructIdent(up, i))
						.unwrap_or_else(|t| t),
				None => return Token::Unfinished,
			}
		}
	}

	fn lex_code(&mut self) -> Option<Token> {
		Some(match self.next()? {
			'!' => match self.next() {
				Some('=') => Token::Ne,
				Some(c) => { self.unget(c); Token::Not },
				None => Token::Not,
			}
			'%' => Token::Percent,
			'&' => match self.next() {
				Some('&') => Token::And,
				_ => return None,
			}
			'|' => match self.next() {
				Some('|') => Token::Or,
				_ => return None,
			}
			'*' => Token::Star,
			'+' => Token::Add,
			'/' => Token::Slash,
			':' => Token::Call,
			'0' => self.num(),
			n@'1'..='9' => self.num_base(10, n),
			'=' => match self.next() {
				Some('=') => Token::Eq,
				Some(c) => { self.unget(c); Token::Assign },
				None => Token::Assign,
			},
			'<' => match self.next() {
				Some('=') => Token::LessEq,
				Some(c) => { self.unget(c); Token::Less },
				None => Token::Less,
			}
			'>' => match self.next() {
				Some('=') => Token::GreatEq,
				Some(c) => { self.unget(c); Token::Great },
				None => Token::Great,
			}
			'-' => match self.next() {
				Some('>') => Token::Func,
				Some(c) => { self.unget(c); Token::Neg },
				None => Token::Neg,
			},
			'.' => match self.next() {
				Some('/') => {
					self.state.push(LexerMode::Path);
					Token::StrOpen(StrType::Relative)
				},
				Some('.') => match self.next() {
					Some('/') => {
						self.state.push(LexerMode::Path);
						Token::StrOpen(StrType::Parent)
					}
					Some(c) => {
						self.unget(c);
						self.relative_reference()
					}
					None => Token::Unfinished,
				},
				Some(c) => { self.unget(c); Token::Dot },
				None => Token::Assign,
			},
			'{' => {
				match self.state.last_mut() {
					Some(&mut LexerMode::Code(ref mut depth)) => *depth += 1,
					other => unreachable!("Unexpected mode {:?}", other)
				};
				Token::DictOpen
			},
			'}' => {
				let depth = match self.state.last_mut() {
					Some(&mut LexerMode::Code(ref mut depth)) => {
						*depth -= 1;
						*depth
					},
					other => unreachable!("Unexpected mode {:?}", other),
				};
				if depth == 0 {
					self.state.pop();
					return self.next_token()
				} else {
					Token::DictClose
				}
			},
			'[' => Token::ListOpen,
			']' => Token::ListClose,
			'(' => Token::ParenOpen,
			')' => Token::ParenClose,
			'"' => {
				let mut depth = 1;
				while if_next!{self, '"' => ()}.is_some() {
					depth += 1;
				}

				let mut indent = String::new();

				if depth > 2 {
					if_next!{self, '\n' => {
						while let Some(c) = self.next() {
							if !c.is_whitespace() {
								self.unget(c);
								break
							}

							indent.push(c)
						}
					}};
				}

				if depth == 2 {
					// "" is an empty string. Since we have already eaten the closing " just finish immediately.
					self.state.push(LexerMode::StringEnd);
				} else {
					self.state.push(LexerMode::String{depth, indent});
				}

				Token::StrOpen(StrType::String)
			},
			'#' => {
				while let Some(c) = self.next() {
					if c == '\n' { return self.next_token() }
				}
				return None
			},
			c if c.is_whitespace() => return self.next_token(),
			c => self.ident(c),
		})
	}

	fn lex_interpolation(&mut self) -> Token {
		self.state.push(LexerMode::Code(1));
		self.lex_code().unwrap_or(Token::Unfinished)
	}

	fn lex_str(&mut self) -> Token {
		let (depth, indent) = match self.state.last().unwrap() {
			LexerMode::String{depth, indent} => (*depth, indent),
			other => unreachable!("Non-strings state {:?}", other),
		};
		let indent = indent.clone(); // Terribly inefficient :(

		let mut s = String::new();

		match self.next() {
			Some('{') => {
				return self.lex_interpolation();
			}
			Some(c) => self.unget(c),
			None => {}
		}

		'main: loop {
			match self.next() {
				Some('"') => {
					for i in 1..depth {
						match self.next() {
							Some('"') => {},
							Some(c) => {
								for _ in 0..i { s.push('"') }
								self.unget(c);
								continue 'main
							},
							None => return Token::Unfinished,
						}
					}
					if_next!{self, '"' => s.push('"')};
					*self.state.last_mut().unwrap() = LexerMode::StringEnd;
					break
				},
				Some('{') => { self.unget('{'); break },
				Some('\\') => match self.next() {
					Some('n') => s.push('\n'),
					Some('t') => s.push('\t'),
					Some('0') => s.push('\0'),
					Some(c@'a'..='z') |
					Some(c@'A'..='Z') |
					Some(c@'1'..='9') =>
						// Unknown escape sequence.
						return self.unexpected(c),
					Some(c) => s.push(c),
					None => return Token::Unfinished,
				},
				Some('\n') => {
					'newline: loop {
						s.push('\n');
						for i in indent.chars() {
							match self.next() {
								Some('"') => {
									for _ in 1..depth {
										match self.next() {
											Some('"') => {},
											Some(c) => return self.unexpected(c),
											None => return Token::Unfinished,
										}
									}
									if_next!{self, '"' => s.push('"')};
									*self.state.last_mut().unwrap() = LexerMode::StringEnd;
									break 'main
								}
								Some('\n') => continue 'newline,
								Some(c) => {
									if c != i {
										return self.unexpected(c)
									}
								}
								None => return Token::Unfinished
							}
						}

						break // Indent consumed.
					}
				}
				Some(c) => s.push(c),
				None => return Token::Unfinished,
			}
		}

		Token::StrChunk(s)
	}

	fn lex_path(&mut self) -> Token {
		match self.next() {
			Some('{') => return self.lex_interpolation(),
			Some(c) => self.unget(c),
			None => {}
		}

		let mut s = String::new();

		loop {
			match self.next() {
				Some('{') => { self.unget('{'); break },
				Some(c@'/') | Some(c@'.') => s.push(c),
				Some(c) if Self::ident_char(c) => s.push(c),
				Some(c) => { self.unget(c); break },
				None => {
					self.unget(' '); // Hack to close path.
					break
				},
			}
		}

		if s.is_empty() {
			let old = self.state.pop();
			debug_assert_eq!(old, Some(LexerMode::Path));
			Token::StrClose
		} else {
			Token::StrChunk(s)
		}

	}
}

impl<Input: Iterator<Item=char>> Iterator for Lexer<Input> {
	type Item = (Token,Loc);

	fn next(&mut self) -> Option<Self::Item> {
		self.next_token().map(|t| (t, self.start_loc))
	}
}

macro_rules! expect_next {
	{$i:ident : $c:expr, $($($p:pat)|+ => $e:expr),+ $(,)* } => {
		match $i.next() {
			$($(Some($p))|+ => { $e }),+
			Some((token, loc)) => {
				return Err(ParseError{
					typ: ErrorType::Unexpected(token),
					msg: $c,
					loc,
				})
			},
			None => {
				return Err(ParseError{
					typ: ErrorType::Unfinished,
					msg: $c,
					loc: Loc{line: 0, col: 0},
				})
			},
		}
	}
}

struct Parser<'a, Input: Iterator<Item=(Token,Loc)>> {
	input: std::iter::Fuse<Input>,
	current: Option<(Token,Loc)>,
	// file: &'a str,
	directory: &'a str,
}

impl<'a, Input: Iterator<Item = (Token, Loc)>> Parser<'a, Input> {
	fn new(path: &'a str, input: Input) -> Self {
		let last_slash = path.rfind('/').map(|i| i + 1).unwrap_or(0);

		Parser{
			// file: path,
			directory: &path[0..last_slash],
			input: input.fuse(),
			current: None,
		}
	}

	fn next(&mut self) -> Option<(Token,Loc)> {
		self.current.take().or_else(|| self.input.next())
	}

	fn peek(&mut self) -> Option<&Token> {
		if self.current.is_none() {
			self.current = self.input.next();
		}
		self.current.as_ref().map(|p| &p.0)
	}

	fn unget(&mut self, t: (Token,Loc)) {
		debug_assert!(self.current.is_none());
		self.current = Some(t);
	}

	fn consume(&mut self, tok: Token) -> Option<Loc> {
		if let Some(t) = self.next() {
			if t.0 == tok {
				return Some(t.1)
			}
			self.unget(t)
		}

		None
	}

	fn document(&mut self) -> ParseResult {
		let is_expr = match self.peek() {
			Some(&Token::DictOpen) |
			Some(&Token::Func) |
			Some(&Token::ListOpen) |
			Some(&Token::ParenOpen) => true,
			_ => false,
		};

		let e = if is_expr { self.expr()? } else { self.document_items()? };

		match self.next() {
			Some((token, loc)) => Err(ParseError{
				typ: ErrorType::Unused(token),
				msg: "At end of document",
				loc,
			}),
			None => Ok(e),
		}
	}

	fn document_items(&mut self) -> ParseResult {
		let mut items = Vec::new();
		while self.peek().is_some() {
			items.push(self.dict_item()?);
		}
		items.sort_unstable_by(crate::dict::AlmostDictElement::sort_cmp);
		Ok(crate::ast::Almost::Dict(items))
	}

	fn dict_items(&mut self) -> ParseResult {
		let mut items = Vec::new();
		while !self.consume(Token::DictClose).is_some() {
			items.push(self.dict_item()?);
		}
		items.sort_unstable_by(crate::dict::AlmostDictElement::sort_cmp);

		Ok(crate::ast::Almost::Dict(items))
	}

	fn dict_item(&mut self) -> Result<crate::dict::AlmostDictElement, ParseError> {
		let ade = expect_next! {self: "dict element",
			(Token::Ident(s), loc) => {
				match self.peek() {
					Some(Token::Assign) | Some(Token::Dot) => {
						let val = self.dict_val()?;
						crate::dict::AlmostDictElement::public(loc, s, val)
					}
					_ => {
						match s.as_str() {
							"assert" => {
								crate::dict::AlmostDictElement::assert(loc, self.expr()?)
							}
							"local" => {
								let name = expect_next!{self: "local name",
									(Token::Ident(name), _) => name,
								};
								expect_next!{self: "local var", (Token::Assign, _) => {}};

								crate::dict::AlmostDictElement::local(loc, name, self.expr()?)
							}
							_ => {
								expect_next!{self: "dict key",
									(Token::Assign, _) => unreachable!("invalid type"),
								};
							}
						}
					}
				}
			},
			(Token::StrOpen(StrType::String), loc) => {
				let s = expect_next!{self: "uoted dict key",
					(Token::StrChunk(s), _) => {
						expect_next!{self: "quoted dict key", (Token::StrClose, _) => {}};
						s
					},
					(Token::StrClose, _) => "".to_owned(),
				};

				let val = self.dict_val()?;
				crate::dict::AlmostDictElement::public(loc, s, val)
			},
		};

		Ok(ade)
	}

	fn dict_val(&mut self) -> ParseResult {
		expect_next!{self: "dict key",
			(Token::Dot, _) => {
				let k = expect_next!{self: "dict key",
					(Token::Ident(k), _) => k,
					(Token::StrOpen(StrType::String), _) => {
						let s = expect_next!{self: "quoted dict key", (Token::StrChunk(s), _) => s};
						expect_next!{self: "quoted dict key", (Token::StrClose, _) => {}};
						s
					},
				};
				let v = self.dict_val()?;
				Ok(crate::ast::Almost::ADict(k, Box::new(v)))
			},
			(Token::Assign, _) => self.expr(),
		}
	}

	fn list_items(&mut self) -> ParseResult {
		let mut items = Vec::new();
		while !self.consume(Token::ListClose).is_some() {
			items.push(self.expr()?);
		}
		Ok(crate::ast::Almost::List(items))
	}

	fn func(&mut self) -> ParseResult {
		let args = self.args()?;
		Ok(crate::ast::Almost::Func(Box::new(crate::func::FuncData{arg: args, body: self.expr()?})))
	}

	fn args(&mut self) -> Result<crate::func::Arg,ParseError> {
		expect_next!{self: "function argument",
			(Token::Ident(name), _) => Ok(crate::func::Arg::One(name)),
			(Token::DictOpen, _) => {
				let mut args = Vec::new();

				while !self.consume(Token::DictClose).is_some() {
					let k = expect_next!{self: "destructure dict key",
						(Token::Ident(k), _) => k,
					};

					if let Some(_) = self.consume(Token::Assign) {
						args.push((k.clone(), false, self.expr()?));
					} else {
						args.push((k.clone(), true, crate::ast::Almost::Nil));
					}
				}

				Ok(crate::func::Arg::Dict(args))
			},
			(Token::ListOpen, _) => {
				let mut args = Vec::new();

				while !self.consume(Token::ListClose).is_some() {
					let k = expect_next!{self: "destructure list key",
						(Token::Ident(k), _) => k,
					};

					if let Some(_) = self.consume(Token::Assign) {
						args.push((k.clone(), false, self.expr()?));
					} else {
						args.push((k.clone(), true, crate::ast::Almost::Nil));
					}
				}

				Ok(crate::func::Arg::List(args))
			},
		}
	}

	fn expr(&mut self) -> ParseResult {
		self.expr_bool()
	}

	fn expr_bool(&mut self) -> ParseResult {
		let mut r = self.expr_cmp()?;

		loop {
			let (op, loc) = match self.next() {
				Some((Token::And, l)) => (crate::ast::BinOp::And, l),
				Some((Token::Or, l)) => (crate::ast::BinOp::Or, l),
				Some(other) => { self.unget(other); break },
				None => break,
			};
			r = crate::ast::Almost::BinOp(loc, op, Box::new(r), Box::new(self.expr_cmp()?));
		}

		Ok(r)
	}

	fn expr_cmp(&mut self) -> ParseResult {
		let r = self.expr_main()?;

		let mut comparisons = Vec::new();
		while let Some((token, loc)) = self.next() {
			let kind = match token {
				Token::Eq => crate::ast::Cmp::Eq,
				Token::Great => crate::ast::Cmp::Great,
				Token::GreatEq => crate::ast::Cmp::GreatEq,
				Token::Ne => crate::ast::Cmp::Ne,
				Token::Less => crate::ast::Cmp::Less,
				Token::LessEq => crate::ast::Cmp::LessEq,
				_ => { self.unget((token, loc)); break },
			};
			comparisons.push((loc, kind, self.expr_main()?));
		}

		if comparisons.is_empty() {
			Ok(r)
		} else {
			Ok(crate::ast::Almost::Cmp(Box::new(r), comparisons))
		}
	}

	fn expr_main(&mut self) -> ParseResult {
		let mut r = self.expr_unary()?;

		loop {
			let (op, loc) = match self.next() {
				Some((Token::Add, l)) => (crate::ast::BinOp::Add, l),
				Some((Token::Call, l)) => (crate::ast::BinOp::Call, l),
				Some((Token::Neg, l)) => (crate::ast::BinOp::Sub, l),
				Some((Token::Percent, l)) => (crate::ast::BinOp::Modulus, l),
				Some((Token::Slash, l)) => (crate::ast::BinOp::Divide, l),
				Some((Token::Star, l)) => (crate::ast::BinOp::Multiply, l),
				Some(other) => { self.unget(other); break },
				None => break,
			};
			r = crate::ast::Almost::BinOp(loc, op, Box::new(r), Box::new(self.expr_unary()?));
		}

		Ok(r)
	}

	fn expr_unary(&mut self) -> ParseResult {
		let (op, loc) = match self.next() {
			Some((Token::Neg, loc)) => (crate::ast::UniOp::Neg, loc),
			Some((Token::Not, loc)) => (crate::ast::UniOp::Not, loc),
			Some(other) => { self.unget(other); return self.expr_index() },
			None => return self.atom(),
		};

		Ok(crate::ast::Almost::UniOp(loc, op, Box::new(self.expr_unary()?)))
	}

	fn expr_index(&mut self) -> ParseResult {
		let mut r = self.atom()?;

		while let Some(loc) = self.consume(Token::Dot) {
			expect_next!{self: "index",
				(Token::Ident(s), _) =>
					r = crate::ast::Almost::BinOp(loc, crate::ast::BinOp::Index, Box::new(r), Box::new(crate::ast::Almost::StrStatic(s))),
				(Token::StrOpen(t), _) =>
					r = crate::ast::Almost::BinOp(loc, crate::ast::BinOp::Index, Box::new(r), Box::new(self.string(t)?)),
			}
		}

		Ok(r)
	}

	fn atom(&mut self) -> ParseResult {
		expect_next!{self: "atom",
			(Token::DictOpen, _) => self.dict_items(),
			(Token::Func, _) => self.func(),
			(Token::Ident(s), loc) => Ok(crate::ast::Almost::Ref(loc, s)),
			(Token::StructIdent(d, s), loc) => Ok(crate::ast::Almost::StructRef(loc, d, s)),
			(Token::ListOpen, _) => self.list_items(),
			(Token::Num(n), _) => Ok(crate::ast::Almost::Num(n)),
			(Token::ParenOpen, _) => {
				let e = self.expr()?;
				expect_next!{self: "closing bracket", (Token::ParenClose, _) => {}};
				Ok(e)
			},
			(Token::StrOpen(t), _) => self.string(t),
		}
	}

	fn string(&mut self, typ: StrType) -> ParseResult {
		let mut pieces = vec![];

		match typ {
			StrType::Relative => {
				pieces.push(crate::ast::StringPart::Lit(self.directory.to_owned()));
			}
			StrType::Parent => {
				pieces.push(crate::ast::StringPart::Lit(self.directory.to_owned() + "../"));
			}
			StrType::String => {},
		}

		// println!("Pieces: {:?}", pieces);

		loop {
			match self.next() {
				Some((Token::StrChunk(s), _)) => pieces.push(crate::ast::StringPart::Lit(s)),
				Some((Token::StrClose, _)) => break,
				Some(t) => {
					self.unget(t);
					pieces.push(crate::ast::StringPart::Exp(self.expr()?))
				},
				None => {
					return Err(ParseError{
						typ: ErrorType::Unfinished,
						msg: "string",
						loc: Loc{line: 0, col: 0},
					})
				},
			}
		}

		if let &[crate::ast::StringPart::Lit(_)] = pieces.as_slice() {
			if let crate::ast::StringPart::Lit(s) = pieces.pop().unwrap() {
				Ok(crate::ast::Almost::StrStatic(s))
			} else {
				unreachable!();
			}
		} else {
			Ok(crate::ast::Almost::Str(pieces))
		}
	}
}

pub fn parse<Input: Iterator<Item=char>>(file: &str, input: Input) -> Result<crate::ast::Almost, crate::Val> {
	let lexer = Lexer::new(input);
	Parser::new(file, lexer).document()
		.map_err(|e| e.into_val(file))
}

pub fn parse_expr<Input: Iterator<Item=char>>(file: &str, input: Input) -> Result<crate::ast::Almost, crate::Val> {
	let lexer = Lexer::new(input);
	// let lexer = lexer.inspect(|t| println!("Token: {:?}", t));
	Parser::new(file, lexer).expr()
		.map_err(|e| e.into_val(file))
}

#[cfg(test)]
mod tests {
	#[test]
	fn num_decimal() {
		assert_eq!(crate::eval("<str>", "(10)").get_int().unwrap(), 10);
		assert_eq!(crate::eval("<str>", "(0d10)").get_int().unwrap(), 10);
		assert_eq!(crate::eval("<str>", "(10.4)").as_f64().unwrap(), 10.4);
		assert_eq!(crate::eval("<str>", "(10.4e6)").get_int().unwrap(), 10400000);
		assert_eq!(crate::eval("<str>", "(10.4e+6)").get_int().unwrap(), 10400000);
		assert_eq!(crate::eval("<str>", "(10.4e-6)").as_f64().unwrap(), 0.0000104);
		assert_eq!(crate::eval("<str>", "(104_000__000_e-6_)").get_int().unwrap(), 104);
		assert_eq!(crate::eval("<str>", "(1M)").get_int().unwrap(), 1_000_000);
		assert_eq!(crate::eval("<str>", "(1ki)").get_int().unwrap(), 1024);
		assert_eq!(crate::eval("<str>", "(4u)").as_f64().unwrap(), 0.000_004);
		assert_eq!(crate::eval("<str>", "(2 - 1)").get_int().unwrap(), 1);
	}

	#[test]
	fn num_binary() {
		assert_eq!(crate::eval("<str>", "(0b10)").get_int().unwrap(), 2);
		assert_eq!(crate::eval("<str>", "(0b10.1)").as_f64().unwrap(), 2.5);
		assert_eq!(crate::eval("<str>", "(0b1M)").get_int().unwrap(), 1_000_000);
		assert_eq!(crate::eval("<str>", "(0b1u)").as_f64().unwrap(), 0.000_001);
	}

	#[test]
	fn num_hex() {
		assert_eq!(crate::eval("<str>", "(0x10)").get_int().unwrap(), 16);
		assert_eq!(crate::eval("<str>", "(0x8.8)").as_f64().unwrap(), 8.5);
		assert_eq!(crate::eval("<str>", "(0x00.1)").as_f64().unwrap(), 0.0625);
	}
}
