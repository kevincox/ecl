use std::rc::Rc;

pub struct List {
	pool: crate::mem::PoolRef,
	data: Vec<Rc<dyn crate::thunk::Thunky>>,
	recursive: std::cell::Cell<bool>,
}

impl List {
	pub fn new(parent: Rc<crate::Parent>, items: Vec<crate::bytecode::Value>) -> crate::Val {
		let pool = crate::mem::Pool::new();
		crate::Val::new(List {
			pool: pool.downgrade(),
			data: items.iter()
				.map(|item| {
					crate::thunk::bytecode(
						parent.clone(),
						item.clone())
				})
				.collect(),
			recursive: false.into(),
		}, pool)
	}

	pub fn of_vals(pool: crate::mem::Pool, data: Vec<Rc<dyn crate::thunk::Thunky>>) -> crate::Val {
		crate::Val::new(List{
			pool: pool.downgrade(),
			data,
			recursive: false.into(),
		}, pool)
	}

	pub fn get(&self, i: usize) -> Option<crate::Val> {
		self.data.get(i).map(|i| i.eval(&self.pool))
	}

	pub fn slice(&self, from: usize, to: usize) -> crate::Val {
		crate::Val::new(List{
			pool: self.pool.clone(),
			data: self.data[from..to].to_vec(),
			recursive: false.into(),
		}, self.pool.clone().upgrade())
	}

	fn iter<'a>(&'a self) -> Box<dyn Iterator<Item=crate::Val> + 'a> {
		Box::new(self.data.iter().map(move |v| v.eval(&self.pool)))
	}
}

impl crate::Value for List {
	fn type_str(&self) -> &'static str { "list" }

	fn pool(&self) -> Option<&crate::mem::PoolRef> {
		Some(&self.pool)
	}

	fn is_empty(&self) -> crate::Val { crate::bool::get(self.data.is_empty()) }
	fn len(&self) -> Option<usize> { Some(self.data.len()) }

	fn index_int(&self, k: usize) -> crate::Val {
		self.data[k].eval(&self.pool)
	}

	fn iter<'a>(&'a self) -> Option<Box<dyn Iterator<Item=crate::Val> + 'a>> {
		Some(self.iter())
	}

	fn reverse_iter<'a>(&'a self) -> Option<Box<dyn Iterator<Item=crate::Val> + 'a>> {
		Some(Box::new(self.data.iter().rev().map(move |v| v.eval(&self.pool))))
	}

	fn reverse(&self) -> crate::Val {
		let mut data: Vec<_> = self.data.clone();
		data.reverse();
		crate::Val::new(List {
			pool: self.pool.clone(),
			data,
			recursive: false.into(),
		}, self.pool.clone().upgrade())
	}

	fn eval(&self) -> Result<(),crate::Val> {
		if self.recursive.get() {
			return Err(crate::Err::new("Recursive structure detected.".into()))
		}
		self.recursive.set(true);

		for val in self.iter() {
			val.eval()?;
		}

		self.recursive.set(false);

		Ok(())
	}
}

impl crate::SameOps for List {
	fn eq(&self, that: &Self) -> crate::Val {
		if self.data.len() != that.data.len() {
			return crate::bool::get(false)
		}

		for (l, r) in self.iter().zip(that.iter()) {
			let ord = l.eq(r);
			if ord.get_bool() != Some(true) {
				return ord
			}
		}

		return crate::bool::get(true)
	}

	fn cmp(&self, that: &Self) -> Result<std::cmp::Ordering,crate::Val> {
		for (l, r) in self.iter().zip(that.iter()) {
			let ord = l.cmp(r)?;
			if ord != std::cmp::Ordering::Equal {
				return Ok(ord)
			}
		}

		let mut l = self.iter();
		let mut r = that.iter();

		loop {
			match (l.next(), r.next()) {
				(None, Some(_)) => return Ok(std::cmp::Ordering::Less),
				(Some(l), Some(r)) => {
					let ord = l.cmp(r)?;
					if ord != std::cmp::Ordering::Equal {
						return Ok(ord)
					}
				}
				(Some(_), None) => return Ok(std::cmp::Ordering::Greater),
				(None, None) => return Ok(std::cmp::Ordering::Equal),
			}
		}
	}

	fn add(&self, that: &Self) -> crate::Val {
		let pool = self.pool.clone().upgrade();
		let mut data = Vec::with_capacity(self.data.len() + that.data.len());
		data.extend(self.data.iter().cloned());
		data.extend(that.data.iter().cloned());
		crate::Val::new(List{
			pool: self.pool.clone(),
			data,
			recursive: false.into(),
		}, pool)
	}
}

impl std::fmt::Debug for List {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		if self.recursive.get() {
			return write!(f, "[<recusrive>]")
		}
		self.recursive.set(true);

		let r = self.data.fmt(f);

		self.recursive.set(false);

		r
	}
}

impl serde::Serialize for List {
	fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
		if self.recursive.get() {
			use serde::ser::Error;
			return Err(S::Error::custom("Recursive structure detected."))
		}
		self.recursive.set(true);

		use serde::ser::SerializeSeq;
		let mut seq = s.serialize_seq(Some(self.data.len()))?;
		for e in &self.data {
			seq.serialize_element(&e.eval(&self.pool))?;
		}
		self.recursive.set(false);
		seq.end()
	}
}
