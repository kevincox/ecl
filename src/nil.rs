#[derive(Debug)]
pub struct Nil;

pub fn get() -> crate::Val {
	crate::WeakVal::Nil.upgrade()
}

impl crate::Value for Nil {
	fn type_str(&self) -> &'static str { "nil" }

	fn to_string(&self) -> crate::Val {
		crate::Val::new_atomic("nil".to_owned())
	}

	fn is_nil(&self) -> bool {
		true
	}

	fn to_bool(&self) -> crate::Val {
		crate::bool::get(false)
	}
}

impl crate::SameOps for Nil {
	fn cmp(&self, _that: &Self) -> Result<std::cmp::Ordering,crate::Val> {
		Ok(std::cmp::Ordering::Equal)
	}
}

impl serde::Serialize for Nil {
	fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
		s.serialize_unit()
	}
}
