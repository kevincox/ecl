#[derive(Debug,PartialEq)]
pub enum StringPart { Lit(String), Exp(Almost) }

#[derive(Copy,Clone,Debug,PartialEq)]
pub enum UniOp {
	Neg,
	Not,
}

impl UniOp {
	fn as_str(&self) -> &'static str {
		match self {
			UniOp::Neg => "-",
			UniOp::Not => "!",
		}
	}
}

#[derive(Copy,Clone,Debug,PartialEq)]
pub enum BinOp {
	Add,
	And,
	Call,
	Divide,
	Index,
	Modulus,
	Multiply,
	Or,
	Sub,
}

impl BinOp {
	fn as_str(&self) -> &'static str {
		match self {
			BinOp::Add => "+",
			BinOp::And => "&&",
			BinOp::Call => ":",
			BinOp::Divide => "/",
			BinOp::Index => ".",
			BinOp::Modulus => "%",
			BinOp::Multiply => "*",
			BinOp::Or => "||",
			BinOp::Sub => "-",
		}
	}
}

#[derive(Debug,PartialEq)]
pub enum Cmp { Eq, Great, GreatEq, Less, LessEq, Ne }

impl Cmp {
	fn str(&self) -> &'static str {
		match self {
			Cmp::Eq => "==",
			Cmp::Great => ">",
			Cmp::GreatEq => ">=",
			Cmp::Less => "<",
			Cmp::LessEq => "<=",
			Cmp::Ne => "!=",
		}
	}
}

impl std::fmt::Display for Cmp {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}", self.str())
	}
}

#[derive(PartialEq)]
pub enum Almost {
	ADict(String,Box<Almost>),
	BinOp(crate::grammar::Loc, BinOp, Box<Almost>, Box<Almost>),
	Cmp(Box<Almost>, Vec<(crate::grammar::Loc, Cmp, Almost)>),
	Dict(Vec<crate::dict::AlmostDictElement>),
	Func(Box<crate::func::FuncData>),
	List(Vec<Almost>),
	Nil,
	Num(num::BigRational),
	Ref(crate::grammar::Loc, String),
	Str(Vec<StringPart>),
	StrStatic(String),
	StructRef(crate::grammar::Loc, usize, String),
	UniOp(crate::grammar::Loc, UniOp, Box<Almost>),
}

impl std::fmt::Debug for Almost {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match *self {
			Almost::BinOp(_, op, ref lhs, ref rhs) => write!(f, "({:?} {} {:?})", lhs, op.as_str(), rhs),
			Almost::ADict(ref key, ref item) => {
				write!(f, "Adict{{{:?} = {:?}}}", key, item)
			}
			Almost::Dict(ref items) => {
				writeln!(f, "{{")?;
				for i in &**items {
					writeln!(f, "\t{:?}", i)?;
				}
				write!(f, "}}")
			}
			Almost::Cmp(ref l, ref comparisons) => {
				write!(f, "({:?}", l)?;
				for (_, kind, val) in comparisons {
					write!(f, " {} {:?}", kind, val)?;
				}
				write!(f, ")")
			}
			Almost::Func(ref fd) => write!(f, "(->{:?} {:?})", fd.arg, fd.body),
			Almost::List(ref items) => {
				writeln!(f, "[")?;
				for item in items {
					writeln!(f, "\t{:?}", item)?;
				}
				write!(f, "]")
			}
			Almost::Nil => write!(f, "nil"),
			Almost::Num(ref n) => write!(f, "{}", n),
			Almost::Ref(_, ref id) => write!(f, "Ref({})", crate::format_key(id)),
			Almost::StructRef(_, d, ref key) => write!(f, "StructRef({})", crate::format_ref(d, &key)),
			Almost::Str(ref parts) => {
				write!(f, "Str(\"")?;
				for part in parts {
					match *part {
						StringPart::Exp(ref s) => write!(f, "{{{:?}}}", s)?,
						StringPart::Lit(ref s) => write!(f, "{}", crate::escape_string_contents(&s))?,
					}
				}
				write!(f, "\"))")
			}
			Almost::StrStatic(ref s) => {
				write!(f, "{}", crate::escape_string(&s))
			}
			Almost::UniOp(_, op, ref v) => write!(f, "{}({:?})", op.as_str(), v),
		}
	}
}
