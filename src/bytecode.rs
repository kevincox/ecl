use byteorder::{self, ByteOrder, ReadBytesExt};
use std;
use std::io::{BufRead};
use std::rc::Rc;

const MAGIC: &[u8] = b"ECL\0v001";
const START_OFFSET: usize = 8;
const DEBUG_OFFSET: usize = 16;

macro_rules! codes {
	( $type:ident $( $item:ident, )* ) => {
		codes!{$type: u8 $( $item, )*}
	};
	( $type:ident : $repr:ident $( $item:ident, )* ) => {
		#[derive(Clone,Copy,Debug)]
		enum $type { $( $item ),* }

		impl $type {
			#[inline(always)]
			fn from(i: $repr) -> Result<Self,crate::Val> {
				$( if i == $type::$item as $repr {
					Ok($type::$item)
				} else )* {
					panic!("Unknown {} 0x{:02x}", stringify!($type), i)
				}
			}

			#[inline]
			fn to(self) -> $repr {
				self as $repr
			}
		}
	};
}

codes!{Op
	Global,
	ADict,
	Add,
	And,
	Call,
	Cmp,
	Dict,
	Divide,
	Eq,
	Func,
	Ge,
	Gt,
	Index,
	Interpolate,
	Le,
	List,
	Lt,
	Modulus,
	Multiply,
	Ne,
	Neg,
	Not,
	Num,
	Or,
	Ref,
	RefRel,
	Str,
	Sub,
}

codes!{DictItem
	Assert,
	Pub,
	Local,
}

codes!{ArgType
	One,
	Dict,
	List,
}

codes!{ArgReq
	Required,
	Optional,
}

type EclByteOrder = byteorder::LittleEndian;

#[derive(Debug)]
struct Scope {
	vars: Vec<(String, usize)>,
	skip_structural: bool,
	parent: Option<Rc<Scope>>,
}

impl Scope {
	fn find(&self, name: &str) -> Option<(usize,usize)> {
		self.vars.iter()
			.find(|i| i.0 == name)
			.map(|i| (0, i.1))
			.or_else(||
				self.parent.as_ref()
					.and_then(|p| p.find(name))
					.map(|(d, id)| (d+1, id)))
	}

	/// Find a variable at a specific depth.
	///
	/// Returns either:
	/// - Ok(id, depth)
	/// - Err(depth)
	///
	/// Note that the returned depth is the "physical" depth, or the actual distance between the expression and the target. This may differ from the input which is the "logical" depth. Notably adict references don't count in the `...var` reference syntax.
	fn find_at(&self, name: &str, mut ups: usize) -> Result<(usize, usize), usize> {
		if self.skip_structural {
			// This frame doesn't count.
			ups += 1;
		}

		if ups == 0 {
			self.vars.iter()
				.find(|i| i.0 == name)
				.map(|i| (i.1, 0))
				.ok_or(0)
		} else {
			self.parent.as_ref()
				.ok_or(ups)
				.and_then(|p| p.find_at(name, ups-1))
				.map(|(id, depth)| (id, depth+1))
				.map_err(|depth| depth+1)
		}
	}
}

enum Compilable {
	Almost(crate::ast::Almost),
	Func(Box<crate::func::FuncData>),
}

struct ToCompile {
	ref_off: usize,
	item: Compilable,
	scope: Rc<Scope>,
}

impl ToCompile {
	fn compile(self, ctx: &mut CompileContext) -> Result<usize, crate::Val> {
		ctx.scope = self.scope;
		match self.item {
			Compilable::Almost(ast) => ctx.compile(ast),
			Compilable::Func(data) => {
				let data = *data;
				let crate::func::FuncData{arg, body} = data;
				let off = match arg {
					crate::func::Arg::One(arg) => {
						let argoff = ctx.out.write_u8(ArgType::One.to());

						let (k, id) = ctx.new_var(arg.clone(), false);
						ctx.out.write_varint(id);

						ctx.scope = Rc::new(Scope{
							vars: vec![(k, id)],
							skip_structural: false,
							parent: Some(ctx.scope.clone()),
						});

						argoff
					}
					crate::func::Arg::Dict(args) => {
						let mut vars = Vec::with_capacity(args.len());

						let args = args.into_iter()
							.map(|(key, required, val)| {
								let (k, id) = ctx.new_var(key.clone(), true);
								vars.push((k, id));
								(key, required, val)
							})
							.collect::<Vec<_>>();

						ctx.scope = Rc::new(Scope{
							vars,
							skip_structural: false,
							parent: Some(ctx.scope.clone()),
						});

						let args = args.into_iter().map(|(key, required, val)| {
								if required {
									(key, Ok(0))
								} else {
									(key, ctx.compile(val))
								}
							})
							.collect::<Vec<_>>();

						let argoff = ctx.out.write_u8(ArgType::Dict.to());
						ctx.out.write_usize(args.len());
						for (key, off) in args {
							let off = off?;

							ctx.out.write_str(&key);
							if off == 0 {
								ctx.out.write_u8(ArgReq::Required.to());
							} else {
								ctx.out.write_u8(ArgReq::Optional.to());
								ctx.out.write_usize(off);
							}
						}

						argoff
					}
					crate::func::Arg::List(args) => {
						let mut vars = Vec::with_capacity(args.len());

						let args = args.into_iter()
							.map(|(key, required, val)| {
								let (k, id) = ctx.new_var(key, false);
								vars.push((k, id));
								(id, required, val)
							})
							.collect::<Vec<_>>();

						ctx.scope = Rc::new(Scope{
							vars,
							skip_structural: false,
							parent: Some(ctx.scope.clone()),
						});

						let args = args.into_iter().map(|(id, required, val)| {
								if required {
									(id, Ok(0))
								} else {
									(id, ctx.compile(val))
								}
							})
							.collect::<Vec<_>>();

						let argoff = ctx.out.write_u8(ArgType::List.to());
						ctx.out.write_varint(args.len());
						for (id, off) in args {
							let off = off?;

							ctx.out.write_varint(id);
							if off == 0 {
								ctx.out.write_u8(ArgReq::Required.to());
							} else {
								ctx.out.write_u8(ArgReq::Optional.to());
								ctx.out.write_usize(off);
							}
						}

						argoff
					}
				};

				ctx.compile(body)?;
				Ok(off)
			}
		}
	}
}

struct Buffer {
	data: Vec<u8>,
}

impl Buffer {
	fn new() -> Self {
		Buffer {
			data: Vec::new(),
		}
	}

	fn len(&self) -> usize {
		self.data.len()
	}

	fn write<T: IntoIterator<Item=u8>>(&mut self, bytes: T) -> usize {
		let start_offset = self.data.len();
		self.data.extend(bytes);
		start_offset
	}

	fn write_u8(&mut self, byte: u8) -> usize {
		self.write(Some(byte))
	}

	fn write_u16(&mut self, n: u16) -> usize {
		let mut buf = [0; 2];
		EclByteOrder::write_u16(&mut buf, n);
		self.write(buf.iter().cloned())
	}

	fn write_u32(&mut self, n: u32) -> usize {
		let mut buf = [0; 4];
		EclByteOrder::write_u32(&mut buf, n);
		self.write(buf.iter().cloned())
	}

	fn write_u64(&mut self, n: u64) -> usize {
		let mut buf = [0; 8];
		EclByteOrder::write_u64(&mut buf, n);
		self.write(buf.iter().cloned())
	}

	fn write_usize(&mut self, n: usize) -> usize {
		self.write_u64(std::convert::TryFrom::try_from(n).unwrap())
	}

	fn write_varint(&mut self, n: usize) -> usize {
		let n: u64 = std::convert::TryFrom::try_from(n).unwrap();
		if n < 1 << 7 {
			self.write_u8((n as u8) << 1 | 0b1)
		} else if n < 1 << 14 {
			self.write_u16((n as u16) << 2 | 0b10)
		} else if n < 1 << 29 {
			self.write_u32((n as u32) << 3 | 0b100)
		} else {
			let r = self.write_u8(0);
			self.write_u64(n);
			r
		}
	}

	fn write_bytes(&mut self, bytes: &[u8]) -> usize {
		let off = self.write_varint(bytes.len());
		self.write(bytes.iter().cloned());
		off
	}

	fn write_str(&mut self, s: &str) -> usize {
		self.write_bytes(s.as_bytes())
	}

	fn reserve_reference(&mut self) -> usize {
		return self.write_u64(0)
	}

	fn write_reference(&mut self, ref_off: usize, code_off: usize) {
		let code_off = std::convert::TryFrom::try_from(code_off).unwrap();
		EclByteOrder::write_u64(&mut self.data[ref_off..][..8], code_off);
	}

	fn to_bytes(self) -> Vec<u8> {
		self.data
	}
}

struct CompileContext {
	out: Buffer,
	last_local: usize,
	compile_queue: Vec<ToCompile>,
	scope: Rc<Scope>,

	debug: Buffer,
	debug_last: usize,
}

impl CompileContext {
	fn new_var(&mut self, name: String, public: bool) -> (String, usize) {
		let id = if public {
			0
		} else {
			self.last_local += 1;
			self.last_local
		};

		(name, id)
	}

	fn compile_outofline(&mut self, scope: Rc<Scope>, node: crate::ast::Almost) -> usize {
		let ref_off = self.out.reserve_reference();
		self.compile_queue.push(ToCompile{
			ref_off,
			item: Compilable::Almost(node),
			scope,
		});
		return ref_off
	}

	fn compile(&mut self, ast: crate::ast::Almost) -> Result<usize,crate::Val> {
		self.compile_expr(ast)
	}

	fn compile_expr(&mut self, ast: crate::ast::Almost) -> Result<usize, crate::Val> {
		match ast {
			crate::ast::Almost::BinOp(loc, op, left, right) => {
				let bytecodeop = match op {
					crate::ast::BinOp::Add => Op::Add,
					crate::ast::BinOp::And => Op::And,
					crate::ast::BinOp::Call => Op::Call,
					crate::ast::BinOp::Divide => Op::Divide,
					crate::ast::BinOp::Index => Op::Index,
					crate::ast::BinOp::Modulus => Op::Modulus,
					crate::ast::BinOp::Multiply => Op::Multiply,
					crate::ast::BinOp::Or => Op::Or,
					crate::ast::BinOp::Sub => Op::Sub,
				};
				self.compile_binary_op(loc, bytecodeop, *left, *right)
			}
			crate::ast::Almost::Cmp(left, mut comparisons) => {
				fn ast_to_op(kind: crate::ast::Cmp) -> Op {
					match kind {
						crate::ast::Cmp::Eq => Op::Eq,
						crate::ast::Cmp::Great => Op::Gt,
						crate::ast::Cmp::GreatEq => Op::Ge,
						crate::ast::Cmp::Less => Op::Lt,
						crate::ast::Cmp::LessEq => Op::Le,
						crate::ast::Cmp::Ne => Op::Ne,
					}
				}

				let last_cmp = comparisons.pop().unwrap();
				if comparisons.is_empty() {
					// Optimized common case.
					let (loc, kind, right) = last_cmp;
					self.compile_binary_op(loc, ast_to_op(kind), *left, right)
				} else {
					let off = self.write_op(Op::Cmp);
					self.compile_expr(*left)?;

					for (loc, kind, val) in comparisons {
						self.write_op(Op::Cmp);
						self.compile_unary_op(loc, ast_to_op(kind), val)?;
					}

					let (loc, kind, val) = last_cmp;
					self.compile_unary_op(loc, ast_to_op(kind), val)?;

					Ok(off)
				}
			}
			crate::ast::Almost::ADict(key, element) => {
				let off = self.write_op(Op::ADict);
				let scope = Rc::new(Scope{
					vars: Vec::new(),
					skip_structural: true,
					parent: Some(self.scope.clone()),
				});
				self.compile_outofline(scope, *element);
				self.out.write_str(&key);
				Ok(off)
			}
			crate::ast::Almost::Dict(elements) => {
				let mut vars = Vec::with_capacity(elements.len());

				for e in &elements {
					if e.is_element() {
						vars.push(self.new_var(e.key.clone(), e.is_public()));
					}
				}

				let scope = Rc::new(Scope{
					vars,
					skip_structural: false,
					parent: Some(self.scope.clone()),
				});

				let off = self.write_op(Op::Dict);
				self.out.write_varint(elements.len());
				for e in elements {
					self.write_debug(e.loc);
					match e.visibility {
						crate::dict::Visibility::Assert => {
							self.out.write_u8(DictItem::Assert.to());
						}
						crate::dict::Visibility::Local => {
							self.out.write_u8(DictItem::Local.to());
							self.out.write_varint(scope.find_at(&e.key, 0).unwrap().0);
						},
						crate::dict::Visibility::Pub => {
							self.out.write_u8(DictItem::Pub.to());
							self.out.write_str(&e.key);
						},
					}
					self.compile_outofline(scope.clone(), e.val);
				}

				Ok(off)
			}
			crate::ast::Almost::Func(data) => {
				let off = self.write_op(Op::Func);
				let refoff = self.out.reserve_reference();
				self.compile_queue.push(ToCompile{
					ref_off: refoff,
					item: Compilable::Func(data),
					scope: self.scope.clone(),
				});
				Ok(off)
			}
			crate::ast::Almost::List(elements) => {
				let off = self.write_op(Op::List);
				self.out.write_varint(elements.len());
				for element in elements {
					let scope = self.scope.clone();
					self.compile_outofline(scope, element);
				}
				Ok(off)
			},
			crate::ast::Almost::Nil => self.compile_global(0),
			crate::ast::Almost::Num(n) => {
				let off = self.write_op(Op::Num);

				let bytes = n.numer().to_signed_bytes_le();
				self.out.write_bytes(&bytes);

				if n.is_integer() {
					self.out.write_varint(0);
				} else {
					let (sign, bytes) = n.denom().to_bytes_le();
					assert_eq!(sign, num::bigint::Sign::Plus);
					self.out.write_bytes(&bytes);
				}

				Ok(off)
			}
			crate::ast::Almost::Ref(loc, name) => {
				self.write_debug(loc);
				if let Some((depth, id)) = self.scope.find(&name) {
					let off = self.write_op(Op::Ref);
					self.out.write_str(&name);
					self.out.write_varint(depth);
					self.out.write_varint(id);
					Ok(off)
				} else if let Some(id) = crate::builtins::builtin_id(&name) {
					let off = self.write_op(Op::Global);
					self.out.write_varint(id);
					Ok(off)
				} else {
					Err(crate::Err::new(format!("{:?} Invalid reference {:?}", loc, name)))
				}
			}
			crate::ast::Almost::StructRef(loc, depth, key) => {
				self.write_debug(loc);
				match self.scope.find_at(&key, depth) {
					Ok((id, depth)) => {
						let off = self.write_op(Op::Ref);
						self.out.write_str(&key);
						self.out.write_varint(depth);
						self.out.write_varint(id);
						Ok(off)
					}
					Err(depth) => {
						let off = self.write_op(Op::RefRel);
						self.out.write_str(&key);
						self.out.write_varint(depth);
						Ok(off)
					}
				}
			}
			crate::ast::Almost::Str(parts) => {
				let off = self.write_op(Op::Interpolate);
				self.out.write_varint(parts.len());
				for part in parts {
					match part {
						crate::ast::StringPart::Exp(s) => {
							self.compile_expr(s)?;
						},
						crate::ast::StringPart::Lit(s) => {
							self.write_op(Op::Str);
							self.out.write_str(&s);
						},
					}
				}
				Ok(off)
			}
			crate::ast::Almost::StrStatic(s) => {
				let off = self.write_op(Op::Str);
				self.out.write_str(&s);
				Ok(off)
			}
			crate::ast::Almost::UniOp(loc, op, v) => {
				let bytecodeop = match op {
					crate::ast::UniOp::Neg => Op::Neg,
					crate::ast::UniOp::Not => Op::Not,
				};
				self.compile_unary_op(loc, bytecodeop, *v)
			}
		}
	}

	fn compile_unary_op(&mut self,
		loc: crate::grammar::Loc,
		op: Op,
		val: crate::ast::Almost,
	) -> Result<usize,crate::Val> {
		self.write_debug(loc);
		let off = self.write_op(op);
		self.compile_expr(val)?;
		Ok(off)
	}

	fn compile_binary_op(&mut self,
		loc: crate::grammar::Loc,
		op: Op,
		left: crate::ast::Almost,
		right: crate::ast::Almost,
	) -> Result<usize,crate::Val> {
		let off = self.compile_unary_op(loc, op, left)?;
		self.compile_expr(right)?;
		Ok(off)
	}

	fn compile_global(&mut self, global: usize) -> Result<usize,crate::Val> {
		let off = self.write_op(Op::Global);
		self.out.write_varint(global);
		Ok(off)
	}

	fn write_op(&mut self, op: Op) -> usize {
		self.out.write_u8(op.to())
	}

	fn write_debug(&mut self, start: crate::grammar::Loc) {
		let new_bytes = self.out.len() - self.debug_last;
		self.debug_last = self.out.len();

		self.debug.write_varint(new_bytes);

		self.debug.write_varint(start.line);
		self.debug.write_varint(start.col);

		// self.write_varint(end.line - start.line);
		// self.write_varint(end.col);
	}
}

pub fn compile_to_vec(ast: crate::ast::Almost) -> Result<Vec<u8>,crate::Val> {
	let mut ctx = CompileContext{
		out: Buffer::new(),
		last_local: 0,
		compile_queue: Vec::new(),
		scope: Rc::new(Scope{
			vars: Vec::new(),
			skip_structural: false,
			parent: None,
		}),

		debug: Buffer::new(),
		debug_last: 0,
	};

	ctx.out.write(MAGIC.iter().cloned());

	ctx.compile_outofline(ctx.scope.clone(), ast);

	let debug_ref = ctx.out.reserve_reference();

	while let Some(compilable) = ctx.compile_queue.pop() {
		let ref_off = compilable.ref_off;
		let code_off = compilable.compile(&mut ctx)?;
		ctx.out.write_reference(ref_off, code_off);
	}

	let debug_off = ctx.out.write(ctx.debug.to_bytes());
	ctx.out.write_varint(ctx.out.len()); // Sentinel after last record.
	ctx.out.write_reference(debug_ref, debug_off);

	Ok(ctx.out.to_bytes())
}

#[derive(Clone,Eq,Ord,PartialEq,PartialOrd)]
pub struct Module {
	pub file: std::path::PathBuf,
	pub code: Vec<u8>,
}

impl Module {
	fn unique_id(&self) -> usize {
		self.code.as_ptr() as usize
	}

	fn start_pc(&self) -> u64 {
		self.read_u64(START_OFFSET)
	}

	fn start_debug(&self) -> u64 {
		self.read_u64(DEBUG_OFFSET)
	}

	fn read_u64(&self, i: usize) -> u64 {
		EclByteOrder::read_u64(&self.code[i..(i+8)])
	}

	pub fn loc(&self, off: usize) -> crate::err::Loc {
		let debug_off = self.start_debug() as usize;
		let mut cursor = std::io::Cursor::new(&self.code[debug_off..]);

		let mut rec_off = 0;
		let loc = loop {
			rec_off += cursor.read_varint();

			if rec_off > off {
				break crate::grammar::Loc { line: 0, col: 0, };
			}

			let loc = crate::grammar::Loc {
				line: cursor.read_varint(),
				col: cursor.read_varint(),
			};
			if rec_off == off {
				break loc;
			}
		};

		crate::err::Loc {
			file: crate::Val::new_atomic(self.file.as_os_str().to_string_lossy().into_owned()),
			line: loc.line,
			col: loc.col,
		}
	}
}

trait CursorExt<'a> {
	fn pos(&self) -> usize;
	fn read_usize(&mut self) -> usize;
	fn read_varint(&mut self) -> usize;
	fn read_ref(&mut self) -> usize;
	fn read_bytes(&mut self) -> &'a [u8];
	fn read_str(&mut self) -> &'a str;
}

impl<'a> CursorExt<'a> for std::io::Cursor<&'a [u8]> {
	fn pos(&self) -> usize {
		self.position() as usize
	}

	#[inline]
	fn read_usize(&mut self) -> usize {
		let n = self.read_u64::<EclByteOrder>().unwrap();
		std::convert::TryFrom::try_from(n).unwrap()
	}

	fn read_varint(&mut self) -> usize {
		let off = self.position() as usize;

		let (bytes, val) = {
			let buf = &self.get_ref()[off..];
			match buf[0].trailing_zeros() {
				0 => (1, (buf[0] >> 1) as u64),
				1 => (2, (EclByteOrder::read_u16(&buf) >> 2) as u64),
				2 => (4, (EclByteOrder::read_u32(&buf) >> 3) as u64),
				8 => (9, EclByteOrder::read_u64(&buf[1..])),
				b => unreachable!("Bits: {}", b),
			}
		};

		self.set_position(off as u64 + bytes);
		std::convert::TryFrom::try_from(val).unwrap()
	}

	fn read_bytes(&mut self) -> &'a [u8] {
		let len = self.read_varint();
		let off = std::convert::TryInto::try_into(self.position()).unwrap();
		self.consume(len);
		&self.get_ref()[off..][..len]
	}

	fn read_str(&mut self) -> &'a str {
		let bytes = self.read_bytes();
		std::str::from_utf8(bytes).unwrap()
	}

	fn read_ref(&mut self) -> usize {
		std::convert::TryInto::try_into(self.read_u64::<EclByteOrder>().unwrap()).unwrap()
	}
}

impl std::fmt::Debug for Module {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Module({:?})", self.file)
	}
}

struct EvalContext<'a> {
	module: &'a Rc<Module>,
	cursor: std::io::Cursor<&'a [u8]>,
	parent: Rc<crate::Parent>,
}

impl<'a> EvalContext<'a> {
	fn eval(mut self) -> crate::Val {
		self.continue_eval()
	}

	fn continue_eval(&mut self) -> crate::Val {
		let off = self.cursor.pos();
		let op = Op::from(self.cursor.read_u8().unwrap())?;
		// eprintln!("EXEC Op::{:?} @ {}", op, self.module.loc(off));
		let r = match op {
			Op::Global => {
				let id = self.cursor.read_varint();
				crate::builtins::get_id(id)
			}
			Op::Add => {
				let (l, r) = self.eval_binop(off, "addition");
				l.add(r)
			}
			Op::And => {
				let left = self.continue_eval()
					.annotate_at_with(|| (self.module.loc(off), "On left side of &&".into()));
				let right = self.continue_eval();
				if left.to_bool()? {
					right.annotate_at_with(|| (self.module.loc(off), "On right side of &&".into()))
				} else {
					left
				}
			}
			Op::Divide => {
				let (l, r) = self.eval_binop(off, "division");
				l.divide(r)
			}
			Op::Or => {
				let left = self.continue_eval()
					.annotate_at_with(|| (self.module.loc(off), "On left side of ||".into()));
				let right = self.continue_eval();
				if left.to_bool()? {
					left
				} else {
					right.annotate_at_with(|| (self.module.loc(off), "On right side of ||".into()))
				}
			}
			Op::Call => {
				let (l, r) = self.eval_binop(off, "call");
				l.call(r).annotate_at(self.module.loc(off), "returned from call")
			}
			Op::Cmp => {
				let mut more = true;
				let mut result = true;
				let mut error = None;
				let mut left = self.continue_eval();
				while more {
					let mut op = Op::from(self.cursor.read_u8().unwrap())?;
					if let Op::Cmp = op {
						op = Op::from(self.cursor.read_u8().unwrap())?;
					} else {
						more = false;
					}

					let right = self.continue_eval();
					if result {
						match self.do_cmp(off, op, &left, &right) {
							Ok(r) => result = r,
							Err(e) => {
								result = false;
								error = Some(e);
							}
						}
						left = right;
					}
				}

				error.unwrap_or_else(|| crate::bool::get(result))
			}
			Op::Eq | Op::Ne | Op::Ge | Op::Gt | Op::Lt | Op::Le => {
				self.eval_cmp(off, op)
			}
			Op::ADict => {
				let childoff = self.cursor.read_u64::<EclByteOrder>().unwrap();
				let key = self.cursor.read_str();
				crate::dict::Dict::new_adict(
					self.parent.clone(),
					key.to_owned(),
					Value::new(self.module.clone(), childoff))
			}
			Op::Dict => {
				let entries_len = self.cursor.read_varint();
				let mut entries = Vec::with_capacity(entries_len);
				for _ in 0..entries_len {
					let pos = self.cursor.pos();
					let source = match DictItem::from(self.cursor.read_u8().unwrap())? {
						DictItem::Assert => {
							let offset = self.cursor.read_u64::<EclByteOrder>().unwrap();
							crate::dict::Source::Assert {
								debug: pos,
								almost: Value::new(self.module.clone(), offset),
							}
						}
						DictItem::Pub => {
							let key = crate::dict::Key::Pub(self.cursor.read_str().to_owned());

							let offset = self.cursor.read_u64::<EclByteOrder>().unwrap();
							crate::dict::Source::Entry {
								key,
								almost: Value::new(self.module.clone(), offset),
							}
						}
						DictItem::Local => {
							let mut id = self.cursor.read_varint();
							id += self.module.unique_id();
							let key = crate::dict::Key::Local(id);

							let offset = self.cursor.read_u64::<EclByteOrder>().unwrap();
							crate::dict::Source::Entry {
								key,
								almost: Value::new(self.module.clone(), offset),
							}
						},
					};
					entries.push(source);
				}

				crate::dict::Dict::new(self.parent.clone(), entries)
			}
			Op::Func => {
				let bodyoff = self.cursor.read_ref();
				crate::func::Func::new(
					self.parent.clone(),
					Func::new(self.module.clone(), bodyoff))
			}
			Op::Index => {
				let (l, r) = self.eval_binop(off, "index");
				l.index(r)
			}
			Op::Interpolate => {
				let mut buf = String::new();
				let mut error = None;

				let chunks = self.cursor.read_varint();
				for _ in 0..chunks {
					let off = self.cursor.pos();
					let v = self.continue_eval()
						.annotate_at_with(|| (
							self.module.loc(off),
							"In string interpolation.".into(),
						));

					match v.to_string().get_str() {
						Ok(s) => { buf += s; }
						Err(e) => { error.get_or_insert(e); }
					}
				}

				error.unwrap_or_else(|| crate::Val::new_atomic(buf))
			}
			Op::List => {
				let len = self.cursor.read_varint();
				let mut items = Vec::with_capacity(len);
				for _ in 0..len {
					let offset = self.cursor.read_u64::<EclByteOrder>().unwrap();
					items.push(Value::new(self.module.clone(), offset));
				}
				crate::list::List::new(self.parent.clone(), items)
			}
			Op::Modulus => {
				let (l, r) = self.eval_binop(off, "modulus");
				l.modulus(r)
			}
			Op::Multiply => {
				let (l, r) = self.eval_binop(off, "multiplication");
				l.multiply(r)
			}
			Op::Neg => {
				self.continue_eval().neg()
			}
			Op::Not => {
				self.continue_eval().not()
			}
			Op::Num => {
				let numerator_bytes = self.cursor.read_bytes();
				let denominator_bytes = self.cursor.read_bytes();

				let numerator = num::BigInt::from_signed_bytes_le(numerator_bytes);
				let denominator = if denominator_bytes.is_empty() {
					1.into()
				} else {
					num::BigInt::from_bytes_le(num::bigint::Sign::Plus, denominator_bytes)
				};

				crate::num::fraction(numerator, denominator)
			}
			Op::Ref => {
				let strkey = self.cursor.read_str();
				let depth = self.cursor.read_varint();
				let mut id = self.cursor.read_varint();
				let key = if id == 0 {
					crate::dict::Key::Pub(strkey.to_owned())
				} else {
					id += self.module.unique_id();
					crate::dict::Key::Local(id)
				};
				// eprintln!("Ref: {:?}@{} {:?}", key, depth, strkey);
				self.parent.structural_lookup(depth, &key)
					.annotate_at_with(|| (
						self.module.loc(off),
						format!("Referenced by {:?}", strkey),
					))
			}
			Op::RefRel => {
				let key = self.cursor.read_str();
				let depth = self.cursor.read_varint();
				let key = crate::dict::Key::Pub(key.to_owned());
				// eprintln!("RefRel: {:?}@{}", key, depth);
				self.parent.structural_lookup(depth, &key)
					.annotate_at_with(|| (
						self.module.loc(off),
						format!("referenced by {}", key),
					))
			}
			Op::Str => {
				let s = crate::str::CodeString {
					module: self.module.clone(),
					len: self.cursor.read_varint(),
					offset: self.cursor.pos(),
				};
				self.cursor.consume(s.len);
				crate::Val::new_atomic(s)
			}
			Op::Sub => {
				let (l, r) = self.eval_binop(off, "subtraction");
				l.subtract(r)
			}
		};
		// eprintln!("DONE Op::{:?} = {:?}", op, r);
		r
	}

	fn eval_binop(&mut self, off: usize, desc: &str) -> (crate::Val, crate::Val) {
		let left = self.continue_eval()
			.annotate_at_with(|| (self.module.loc(off), format!("On left side of {}", desc)));
		let right = self.continue_eval()
			.annotate_at_with(|| (self.module.loc(off), format!("On right side of {}", desc)));
		(left, right)
	}

	fn assert_binop(&mut self,
		assert_loc: usize,
		off: u64,
		desc: &str,
		f: impl FnOnce(&crate::Val, crate::Val) -> crate::Val,
	) -> Result<(), crate::Val> {
		let (left, right) = self.eval_binop(off as usize, desc);
		let val = f(&left, right.clone())
			.annotate_at(self.module.loc(assert_loc), "Evaluating assertion.")
			.to_bool()?;
		if !val {
			Err(crate::Err::new_at(self.module.loc(assert_loc),
				format!("Assertion failed left {} right:\nleft:  {:?}\nright: {:?}", desc, left, right)))
		} else {
			Ok(())
		}
	}

	fn do_cmp(&mut self,
		off: usize,
		op: Op,
		left: &crate::Val,
		right: &crate::Val,
	) -> Result<bool, crate::Val> {
		let (desc, f): (_, &dyn Fn(crate::Val, crate::Val) -> Result<bool, crate::Val>) = match op {
			Op::Eq => ("==", &|l, r| l.eq(r).to_bool()),
			Op::Ge => (">=", &|l, r| Ok(l.cmp(r)? != std::cmp::Ordering::Less)),
			Op::Gt => (">", &|l, r| Ok(l.cmp(r)? == std::cmp::Ordering::Greater)),
			Op::Le => ("<=", &|l, r| Ok(l.cmp(r)? != std::cmp::Ordering::Greater)),
			Op::Lt => ("<", &|l, r| Ok(l.cmp(r)? == std::cmp::Ordering::Less)),
			Op::Ne => ("==", &|l, r| l.ne(r).to_bool()),
			_ => unreachable!("Unexpected {:?}", op),
		};

		let left = left.annotate_at_with(|| (self.module.loc(off), format!("On left side of {}", desc)));
		let right = right.annotate_at_with(|| (self.module.loc(off), format!("On right side of {}", desc)));

		f(left, right).map_err(|e| e.annotate_at(self.module.loc(off), &format!("At {}", desc)))
	}

	fn eval_cmp(&mut self,
		off: usize,
		op: Op,
	) -> crate::Val {
		let left = self.continue_eval();
		let right = self.continue_eval();

		crate::bool::get(self.do_cmp(off, op, &left, &right)?)
	}

	fn assert_cmp(&mut self,
		assert_loc: usize,
		off: u64,
		desc: &str,
		f: impl FnOnce(std::cmp::Ordering) -> bool,
	) -> Result<(), crate::Val> {
		let loc = self.module.loc(off as usize);
		self.assert_binop(assert_loc, off, desc, |left, right| {
			let ord = left.cmp(right.clone())
				.map_err(|e| e.annotate_at(loc, &format!("At {}", desc)))?;

			crate::bool::get(f(ord))
		})
	}
}

pub fn eval(source: impl Into<std::path::PathBuf>, code: Vec<u8>) -> crate::Val {
	let module = Rc::new(Module{
		file: source.into(),
		code,
	});

	let pc = module.start_pc();
	let parent = Rc::new(crate::Parent {
		parent: crate::nil::get().downgrade(&crate::mem::Pool::new()),
		grandparent: None,
	});
	Value::new(module, pc).eval(parent)
}

#[derive(Clone,Debug,Eq,PartialEq)]
pub struct Value {
	pub module: Rc<Module>,
	offset: u64,
}

impl Value {
	fn new(module: Rc<Module>, offset: u64) -> Self {
		Value{module, offset}
	}

	pub fn eval(&self, parent: Rc<crate::Parent>) -> crate::Val {
		let mut cursor = std::io::Cursor::new(&self.module.code[..]);
		cursor.set_position(self.offset);
		EvalContext { module: &self.module, cursor, parent }.eval()
	}

	pub fn eval_assert(&self, parent: Rc<crate::Parent>, debug: usize) -> Result<(), crate::Val> {
		let mut cursor = std::io::Cursor::new(&self.module.code[..]);
		cursor.set_position(self.offset);
		let op = Op::from(cursor.read_u8().unwrap())?;
		let mut context = EvalContext { module: &self.module, cursor, parent };

		match op {
			Op::Ge => {
				context.assert_cmp(debug, self.offset, ">=", |o| o != std::cmp::Ordering::Less)
			}
			Op::Gt => {
				context.assert_cmp(debug, self.offset, ">", |o| o == std::cmp::Ordering::Greater)
			}
			Op::Eq => context.assert_binop(debug, self.offset, "==", crate::Val::eq),
			Op::Lt => {
				context.assert_cmp(debug, self.offset, "<", |o| o == std::cmp::Ordering::Less)
			}
			Op::Le => {
				context.assert_cmp(debug, self.offset, "<=", |o| o != std::cmp::Ordering::Greater)
			}
			Op::Ne => context.assert_binop(debug, self.offset, "!=", crate::Val::ne),
			_ => {
				context.cursor.set_position(self.offset as u64);
				let val = context.eval();
				if val.is_nil().unwrap_or(false) {
					let loc = self.module.loc(debug);
					return Err(crate::Err::new_at(loc, "Assertion nil".into()))
				}

				let val = val
					.to_bool()
					.map_err(|e| e.annotate_at(self.module.loc(debug), "Evaluating assertion condition"))?;

				if !val {
					let loc = self.module.loc(debug);
					Err(crate::Err::new_at(loc, "Assertion failed".into()))
				} else {
					Ok(())
				}
			}
		}
	}
}

#[derive(Clone,Debug)]
pub struct Func {
	module: std::rc::Rc<Module>,
	offset: usize,
}

impl Func {
	fn new(module: Rc<Module>, offset: usize) -> Self {
		Func{module, offset}
	}

	pub fn call(&self, parent: Rc<crate::Parent>, arg: crate::Val) -> crate::Val {
		let mut cursor = std::io::Cursor::new(&self.module.code[..]);
		cursor.set_position(self.offset as u64);

		let args = crate::dict::Dict::new(parent.clone(), Vec::new());
		let dict = args.clone();
		let dict = dict.downcast_ref::<crate::dict::Dict>().unwrap();

		if let Some(pool) = parent.pool() {
			dict.pool.merge(&pool);
		}

		let parent = Rc::new(crate::Parent {
			parent: args.clone().downgrade(&dict.pool),
			grandparent: Some(parent),
		});

		match ArgType::from(cursor.read_u8().unwrap())? {
			ArgType::One => {
				let mut id = cursor.read_varint();
				id += self.module.unique_id();
				dict._set_val(crate::dict::Key::Local(id), crate::thunk::shim(arg.downgrade(&dict.pool)));
			}
			ArgType::Dict => {
				use crate::Value;

				let sourcedict = match arg.downcast_ref::<crate::dict::Dict>() {
					Ok(d) => d,
					Err(_) => return crate::Err::new(format!(
						"Function must be called with dict, called with {:?}",
						arg)),
				};
				let mut unused_args = sourcedict.len().unwrap();

				let len = cursor.read_usize();
				for _ in 0..len {
					let key = cursor.read_str();
					let passed = sourcedict.try_index(&crate::dict::Key::Pub(key.to_owned()))
						.map(|v| v.annotate("Looking up argument value"));
					let val = match ArgReq::from(cursor.read_u8().unwrap())? {
						ArgReq::Required => match passed {
							Some(val) => {
								unused_args -= 1;
								crate::thunk::shim(val.downgrade(&dict.pool))
							}
							None => return crate::Err::new(format!(
								"Required argument {:?} not set in {:?}",
								key, sourcedict)),
						}
						ArgReq::Optional => {
							let off = cursor.read_u64::<EclByteOrder>().unwrap();
							match passed {
								Some(v) => {
									unused_args -= 1;
									crate::thunk::shim(v.downgrade(&dict.pool))
								}
								None => {
									let value = self::Value::new(self.module.clone(), off);
									crate::thunk::bytecode(parent.clone(), value)
								}
							}
						}
					};
					dict._set_val(crate::dict::Key::Pub(key.to_owned()), val)
				}

				if unused_args != 0 {
					return crate::Err::new(format!(
						"Function called with {} unused arguments.", unused_args));
				}
			}
			ArgType::List => {
				use crate::Value;

				let list = match arg.downcast_ref::<crate::list::List>() {
					Ok(l) => l,
					Err(_) => return crate::Err::new(format!(
						"Function must be called with list, called with {:?}",
						arg)),
				};
				let arg_len = list.len().unwrap();

				let len = cursor.read_varint();
				if arg_len > len {
					return crate::Err::new(format!(
						"Function called with too many arguments, expected {} got {}",
						len, arg_len))
				}

				for i in 0..len {
					let mut id = cursor.read_varint();
					id += self.module.unique_id();

					let passed = list.get(i);
					let val = match ArgReq::from(cursor.read_u8().unwrap())? {
						ArgReq::Required => match passed {
							Some(val) => {
								crate::thunk::shim(val.downgrade(&dict.pool))
							}
							None => return crate::Err::new(format!(
								"Required argument {} not provided.", i)),
						}
						ArgReq::Optional => {
							let off = cursor.read_u64::<EclByteOrder>().unwrap();
							passed
								.map(|v| crate::thunk::shim(v.downgrade(&dict.pool)))
								.unwrap_or_else(|| {
									let value = self::Value::new(self.module.clone(), off);
									crate::thunk::bytecode(parent.clone(), value)
								})
						}
					};
					dict._set_val(crate::dict::Key::Local(id), val)
				}
			}
		};

		let v = EvalContext { module: &self.module, cursor, parent }.eval();
		v.merge_pool(&dict.pool);
		v
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn compile_global() {
		assert_eq!(
			compile_to_vec(crate::ast::Almost::Nil).unwrap(),
			b"ECL\0v001\x18\0\0\0\0\0\0\0\x1A\0\0\0\0\0\0\0\x00\x01\x35".to_vec());
	}
}
