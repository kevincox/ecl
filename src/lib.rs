#![feature(try_trait_v2)]

#[macro_use] extern crate erased_serde;

mod ast;
mod bool;
mod builtins;
mod bytecode;
mod dict;
mod err;
mod fs;
mod func;
mod grammar;
mod list;
mod mem;
mod nil;
mod null;
mod num;
mod str;
mod thunk;
mod val;
pub mod lines;

use err::Err;
pub use val::Val;
use val::*;

pub use builtins::internal_reset_leak_count;

fn i_promise_this_will_stay_alive<T: ?Sized>(v: &T) -> &'static T {
	unsafe { std::mem::transmute(v) }
}

pub fn eval(source: &str, doc: &str) -> Val {
	assert!(source.find('/').is_none(), "Non-file source can't have a path.");
	grammar::parse(source, doc.chars())
		.map_err(|e| err::Err::new(format!("Failed to parse string: {:?}", e)))
		.and_then(bytecode::compile_to_vec)
		.map(|code| bytecode::eval(source, code))
		.unwrap_or_else(|e| e)
}

pub fn parse_file(path: &str) -> Result<ast::Almost,Val> {
	let buf = fs::read(path)?;
	let string = String::from_utf8(buf)
		.map_err(|e| err::Err::new(format!("{:?} contains invalid utf8: {}", path, e)))?;
	let ast = grammar::parse(path, string.chars())?;
	Ok(ast)
}

pub fn eval_file(path: &str) -> Val {
	parse_file(path)
		.and_then(bytecode::compile_to_vec)
		.map(|code| bytecode::eval(path, code))
		.unwrap_or_else(|e| e)
}

pub fn hacky_parse_func(source: &str, name: String, code: &str) -> Val
{
	assert!(source.find('/').is_none(), "Non-file source can't have a path.");

	grammar::parse_expr(source, code.chars())
		.map_err(|e| err::Err::new(format!("Failed to parse {:?}: {:?}", source, e)))
		.map(|ast| {
			ast::Almost::Func(Box::new(func::FuncData{
				arg: func::Arg::One(name),
				body: ast,
			}))
		})
		.and_then(bytecode::compile_to_vec)
		.map(|code| bytecode::eval(source, code))
		.unwrap_or_else(|e| e)
}

pub fn dump_ast(doc: &str) -> Result<(), crate::Val> {
	let almost = grammar::parse("", doc.chars())?;
	println!("{:?}", almost);
	Ok(())
}

fn do_escape_string_contents(s: &str, r: &mut String) {
	for c in s.chars() {
		match c {
			'"'  => r.push_str("\\\""),
			'{'  => r.push_str("\\{"),
			'\0' => r.push_str("\\0"),
			'\n' => r.push_str("\\n"),
			'\t' => r.push_str("\\t"),
			_ => r.push(c),
		}
	}
}

fn escape_string_contents(s: &str) -> String {
	let mut r = String::with_capacity(s.len());
	do_escape_string_contents(s, &mut r);
	r
}


fn escape_string(s: &str) -> String {
	let mut r = String::with_capacity(s.len() + 2);
	r.push('"');
	do_escape_string_contents(s, &mut r);
	r.push('"');
	r
}

fn format_key(s: &str) -> String {
	#[ctor::ctor]
	static RE: regex::Regex = regex::Regex::new("^[A-Za-z0-9-_]+$").unwrap();

	if RE.is_match(s) {
		s.to_owned()
	} else {
		escape_string(s)
	}
}

fn format_ref(depth: usize, ident: &str) -> String {
	".".repeat(depth+1) + ident
}
