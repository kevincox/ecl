pub type Result<T> = std::result::Result<T,Error>;

#[derive(Debug)]
pub enum Error {
	Custom(String),
	Io(std::io::Error),
	// InvalidKey(String),
	NonStringKey(String),
	NotImplemented(&'static str),
}

impl std::fmt::Display for Error {
	// This trait requires `fmt` with this exact signature.
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{:?}", self)
	}
}

impl std::error::Error for Error {
	fn description(&self) -> &str {
		"Error serializing to lines"
	}
}

impl serde::ser::Error for Error {
	fn custom<T: std::fmt::Display>(msg: T) -> Self {
		Error::Custom(format!("{}", msg))
	}
}

pub struct Serializer<W: std::io::Write> {
	out: W,
	path: String,
	path_seperators: Vec<usize>,
}

impl<W: std::io::Write> Serializer<W> {
	pub fn new(out: W) -> Self {
		Serializer { out: out, path: String::new(), path_seperators: Vec::new() }
	}

	fn write(&mut self, typ: &str, data: &str) -> Result<()> {
		writeln!(self.out, "{}\t{}\t{}", self.path, typ, data).map_err(|e| Error::Io(e))
	}

	fn push(&mut self, segment: &str) {
		let prev_len = self.path.len();
		if !self.path_seperators.is_empty() {
			self.path.push('.');
		}
		self.path += &super::format_key(segment);
		self.path_seperators.push(prev_len);
	}

	fn pop(&mut self) {
		self.path.truncate(self.path_seperators.pop().unwrap())
	}
}

impl<'a, W: std::io::Write + 'a> serde::Serializer for &'a mut Serializer<W> {
	type Ok = ();
	type Error = Error;

	type SerializeMap = SerializeMap<'a, W>;
	type SerializeSeq = SerializeSeq<'a, W>;
	type SerializeStruct = serde::ser::Impossible<(), Error>;
	type SerializeStructVariant = serde::ser::Impossible<(), Error>;
	type SerializeTuple = serde::ser::Impossible<(), Error>;
	type SerializeTupleStruct = serde::ser::Impossible<(), Error>;
	type SerializeTupleVariant = serde::ser::Impossible<(), Error>;

	fn serialize_bool(self, value: bool) -> Result<()> {
		self.write("BOOL", if value { "true" } else { "false" })
	}

	fn serialize_i8(self, _value: i8) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_i16(self, _value: i16) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_i32(self, _value: i32) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_i64(self, value: i64) -> Result<()> {
		self.write("NUM", &value.to_string())
	}

	fn serialize_u8(self, _value: u8) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_u16(self, _value: u16) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_u32(self, _value: u32) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_u64(self, _value: u64) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_f32(self, _value: f32) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_f64(self, value: f64) -> Result<()> {
		self.write("NUM", &value.to_string())
	}

	fn serialize_char(self, _value: char) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_str(self, value: &str) -> Result<()> {
		self.write("STR", &super::escape_string(value))
	}

	fn serialize_bytes(self, _value: &[u8]) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_unit(self) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_unit_struct(self, _name: &'static str) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str
	) -> Result<()> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_newtype_struct<T: serde::Serialize + ?Sized>(
		self, _name: &'static str, _value: &T) -> Result<()>
	{
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_value: &T,
	) -> Result<()>
		where T: serde::Serialize + ?Sized,
	{
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_none(self) -> Result<()> {
		self.write("NIL", "nil")
	}

	fn serialize_some<T>(self, _value: &T) -> Result<()>
		where T: serde::Serialize + ?Sized,
	{
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
		self.write("LIST", "[")?;
		Ok(SerializeSeq(self, 0))
	}

	fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize
	) -> Result<Self::SerializeTupleStruct> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize
	) -> Result<Self::SerializeTupleVariant> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
		self.write("DICT", "{")?;
		Ok(SerializeMap(self))
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize
	) -> Result<Self::SerializeStruct> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize
	) -> Result<Self::SerializeStructVariant> {
		Err(Error::NotImplemented(concat!(file!(), ":", line!())))
	}
}

pub struct SerializeSeq<'a, W: std::io::Write + 'a>(&'a mut Serializer<W>, usize);

impl<'a, W: std::io::Write> serde::ser::SerializeSeq for SerializeSeq<'a, W> {
	type Ok = ();
	type Error = Error;

	fn serialize_element<T: serde::Serialize + ?Sized>(&mut self, val: &T) -> Result<()> {
		self.1 += 1;
		self.0.push(&self.1.to_string());
		val.serialize(&mut*self.0)?;
		self.0.pop();
		Ok(())
	}

	fn end(self) -> Result<()> {
		self.0.write("LIST", "]")
	}
}

pub struct SerializeMap<'a, W: std::io::Write + 'a>(&'a mut Serializer<W>);

impl<'a, W: std::io::Write> serde::ser::SerializeMap for SerializeMap<'a, W> {
	type Ok = ();
	type Error = Error;

	fn serialize_key<T: serde::Serialize + ?Sized>(&mut self, key: &T) -> Result<()> {
		key.serialize(KeySerializer(self.0))
	}

	fn serialize_value<T: serde::Serialize + ?Sized>(&mut self, val: &T) -> Result<()> {
		val.serialize(&mut*self.0)?;
		self.0.pop();
		Ok(())
	}

	fn end(self) -> Result<()> {
		self.0.write("DICT", "}")
	}
}


struct KeySerializer<'a, W: std::io::Write + 'a>(&'a mut Serializer<W>);

impl<'a, W: std::io::Write> serde::Serializer for KeySerializer<'a, W> {
	type Ok = ();
	type Error = Error;

	type SerializeMap = serde::ser::Impossible<(), Error>;
	type SerializeSeq = serde::ser::Impossible<(), Error>;
	type SerializeStruct = serde::ser::Impossible<(), Error>;
	type SerializeStructVariant = serde::ser::Impossible<(), Error>;
	type SerializeTuple = serde::ser::Impossible<(), Error>;
	type SerializeTupleStruct = serde::ser::Impossible<(), Error>;
	type SerializeTupleVariant = serde::ser::Impossible<(), Error>;

	fn serialize_bool(self, _value: bool) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_i8(self, _value: i8) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_i16(self, _value: i16) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_i32(self, _value: i32) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_i64(self, _value: i64) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_u8(self, _value: u8) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_u16(self, _value: u16) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_u32(self, _value: u32) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_u64(self, _value: u64) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_f32(self, _value: f32) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_f64(self, _value: f64) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_char(self, _value: char) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 

	fn serialize_str(self, value: &str) -> Result<()> {
		self.0.push(value);
		Ok(())
	}

	fn serialize_bytes(self, _value: &[u8]) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) } 
	fn serialize_unit(self) -> Result<()> { Err(Error::NonStringKey(self.0.path.clone())) }

	fn serialize_unit_struct(self, _name: &'static str) -> Result<()> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str
	) -> Result<()> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_newtype_struct<T>(self, _name: &'static str, _value: &T) -> Result<()>
		where T: serde::Serialize + ?Sized,
	{
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_value: &T
	) -> Result<()>
		where T: serde::Serialize + ?Sized,
	{
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_none(self) -> Result<()> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_some<T>(self, _value: &T) -> Result<()>
		where T: serde::Serialize + ?Sized,
	{
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize
	) -> Result<Self::SerializeTupleStruct> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize
	) -> Result<Self::SerializeTupleVariant> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize
	) -> Result<Self::SerializeStruct> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize
	) -> Result<Self::SerializeStructVariant> {
		Err(Error::NonStringKey(self.0.path.clone()))
	}
}
