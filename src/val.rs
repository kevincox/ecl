use std::convert::TryInto;

pub trait Value:
	erased_serde::Serialize +
	std::any::Any +
	std::fmt::Debug +
	std::any::Any +
	std::fmt::Debug +
	SameOpsTrait +
	'static
{
	fn type_str(&self) -> &'static str;
	fn pool(&self) -> Option<&crate::mem::PoolRef> { None }

	fn eval(&self) -> Result<(),Val> { Ok(()) }
	fn is_err(&self) -> bool { false }
	fn is_nil(&self) -> bool { false }
	fn is_empty(&self) -> Val { crate::Err::new(format!("Can't check if {:?} is empty", self)) }
	fn len(&self) -> Option<usize> { None }
	fn index_int(&self, _k: usize) -> Val { crate::Err::new(format!("Can't index {:?} with an int", self)) }
	fn index_str(&self, _k: &str) -> Val { crate::Err::new(format!("Can't index {:?} with string", self)) }
	fn structural_lookup(&self, _depth: usize, key: &crate::dict::Key) -> Val {
		crate::Err::new(format!("Can't lookup {} in {:?}", key, self))
	}
	fn get_str(&self) -> Option<&str> { None }
	fn get_num(&self) -> Option<&num::BigRational> { None }
	fn to_string(&self) -> Val { crate::Err::new(format!("Can't turn {:?} into a string", self)) }
	fn to_bool(&self) -> Val { crate::bool::get(true) }
	fn get_bool(&self) -> Option<bool> { None }
	fn neg(&self) -> Val { crate::Err::new(format!("Can't negate {:?}", self)) }
	fn call(&self, arg: Val) -> Val { crate::Err::new(format!("Can't call {:?} with {:?}", self, arg)) }
	fn iter<'a>(&'a self) -> Option<Box<dyn Iterator<Item=Val> + 'a>> { None }
	fn iter_dict<'a>(&'a self) -> Option<Box<dyn Iterator<Item=(&'a str, Val)> + 'a>> { None }
	fn reverse_iter<'a>(&'a self) -> Option<Box<dyn Iterator<Item=Val> + 'a>> { None }
	fn reverse(&self) -> Val { crate::Err::new(format!("Can't reverse {:?}", self)) }
}

serialize_trait_object!(Value);

pub trait SameOps: std::fmt::Debug {
	fn add(&self, that: &Self) -> Val {
		crate::Err::new(format!("Can't add {:?} and {:?}", self, that))
	}

	fn subtract(&self, that: &Self) -> Val {
		crate::Err::new(format!("Can't subtract {:?} and {:?}", self, that))
	}

	fn eq(&self, that: &Self) -> Val {
		crate::bool::get(self.cmp(that)? == std::cmp::Ordering::Equal)
	}

	fn cmp(&self, that: &Self) -> Result<std::cmp::Ordering,Val> {
		Err(crate::Err::new(format!("Can't compare {:?} and {:?}", self, that)))
	}
}

pub trait SameOpsTrait: std::fmt::Debug {
	fn as_any(&self) -> &dyn std::any::Any;

	fn add(&self, that: &dyn Value) -> Val {
		crate::Err::new(format!("Can't add {:?} and {:?}", self, that))
	}

	fn subtract(&self, that: &dyn Value) -> Val {
		crate::Err::new(format!("Can't subtract {:?} and {:?}", self, that))
	}

	fn eq(&self, that: &dyn Value) -> Val;

	fn cmp(&self, that: &dyn Value) -> Result<std::cmp::Ordering,Val> {
		Err(crate::Err::new(format!("Can't compare {:?} and {:?}", self, that)))
	}
}

impl<T: SameOps + Value> SameOpsTrait for T {
	fn as_any(&self) -> &dyn std::any::Any { self }

	fn add(&self, that: &dyn Value) -> Val {
		if let Some(that) = that.as_any().downcast_ref::<Self>() {
			SameOps::add(self, that)
		} else {
			crate::Err::new(format!("Can't add {:?} and {:?}", self, that))
		}
	}

	fn subtract(&self, that: &dyn Value) -> Val {
		if let Some(that) = that.as_any().downcast_ref::<Self>() {
			SameOps::subtract(self, that)
		} else {
			crate::Err::new(format!("Can't subtract {:?} and {:?}", self, that))
		}
	}

	fn eq(&self, that: &dyn Value) -> Val {
		if let Some(that) = that.as_any().downcast_ref::<Self>() {
			SameOps::eq(self, that)
		} else {
			crate::bool::get(false)
		}
	}

	fn cmp(&self, that: &dyn Value) -> Result<std::cmp::Ordering,Val> {
		if let Some(that) = that.as_any().downcast_ref::<Self>() {
			SameOps::cmp(self, that)
		} else {
			Err(crate::Err::new(format!("Can't compare values of different types {:?} and {:?}", self, that)))
		}
	}
}

#[derive(Clone,Debug)]
pub struct Parent {
	pub parent: WeakVal,
	pub grandparent: Option<std::rc::Rc<Parent>>,
}

impl Parent {
	pub fn pool(&self) -> Option<crate::mem::PoolRef> {
		self.parent.upgrade().deref().pool().cloned()
	}

	pub fn structural_lookup(&self, depth: usize, key: &crate::dict::Key) -> crate::Val {
		match depth {
			0 => self.parent.upgrade().deref().structural_lookup(0, key),
			n => match self.grandparent.as_ref() {
				Some(v) => v.structural_lookup(n-1, key),
				None => crate::Err::new(format!("Can't access parent of {:?}", self.parent.upgrade())),
			}
		}
	}
}

#[derive(Clone)]
pub struct Val {
	pool: Option<crate::mem::Pool>,
	weak: WeakVal,
}

impl Val {
	pub fn new<T: Value + Sized>(
		value: T,
		pool: crate::mem::Pool,
	) -> Self {
		let rc: std::rc::Rc<dyn Value> = std::rc::Rc::new(value);
		let weak = WeakVal::Ref(std::rc::Rc::downgrade(&rc));
		pool.push(rc);
		Val{
			pool: Some(pool),
			weak,
		}
	}

	pub fn new_atomic<T: Value + Sized>(value: T) -> Val {
		debug_assert!(value.pool().is_none());
		Val{
			pool: None,
			weak: WeakVal::Atomic(std::rc::Rc::new(value)),
		}
	}
	
	fn deref(&self) -> &dyn Value {
		self.weak.deref()
	}

	pub fn value(&self) -> Result<&dyn Value,Val> {
		let this = self.deref();
		if this.is_err() { Err(self.clone()) } else { Ok(this) }
	}

	pub fn downgrade(self, pool: &crate::mem::PoolRef) -> WeakVal {
		self.merge_pool(pool);
		self.weak
	}

	pub fn merge_pool(&self, pool: &crate::mem::PoolRef) {
		if let Some(p) = &self.pool {
			p.merge(pool);
		}
	}

	pub fn annotate(&self, msg: &str) -> Val {
		self.annotate_at(Default::default(), msg)
	}

	pub fn annotate_at(&self, loc: crate::err::Loc, msg: &str) -> Val {
		self.annotate_at_with(|| (loc, msg.into()))
	}

	pub fn annotate_with(&self, f: impl FnOnce() -> String) -> Val {
		self.annotate_at_with(|| (Default::default(), f()))
	}

	pub fn annotate_at_with(&self, f: impl FnOnce() -> (crate::err::Loc, String)) -> Val {
		if self.is_err() {
			let (loc, msg) = f();
			crate::Err::new_from_at(self.clone(), loc, msg)
		} else {
			self.clone()
		}
	}

	pub fn downcast_ref<T: 'static>(&self) -> Result<&T, Val> {
		if let Some(v) = self.deref().as_any().downcast_ref::<T>() {
			Ok(crate::i_promise_this_will_stay_alive(v))
		} else if self.is_err() {
			Err(crate::Err::new_from(self.clone(), format!("Expected {} got error", "")))
		} else {
			Err(crate::Err::new(format!("Expected {} got {:?}", "", self)))
		}
	}

	pub fn type_str(&self) -> &'static str {
		self.deref().type_str()
	}

	pub fn get_bool(&self) -> Option<bool> {
		self.value().unwrap().get_bool()
	}

	pub fn get_num(&self) -> Result<&num::BigRational, Val> {
		let v = self.value()?;
		v.get_num().ok_or_else(|| crate::Err::new(format!("{:?} is not a number.", v)))
	}

	pub fn get_int(&self) -> Result<i64, Val> {
		let n = self.get_num()?;
		if !n.is_integer() {
			return Err(crate::Err::new(format!("{} is not an integer.", n)))
		}
		match n.numer().try_into() {
			Ok(n) => Ok(n),
			Err(e) => Err(crate::Err::new(format!("{} cannot be converted to an integer {:?}.", n, e))),
		}
	}

	pub fn as_f64(&self) -> Result<f64, Val> {
		let n = self.get_num()?;

		use num::ToPrimitive;
		Ok(n.numer().to_f64().unwrap() / n.denom().to_f64().unwrap())
	}

	pub fn is_empty(&self) -> Val {
		self.value().unwrap().is_empty()
	}

	pub fn is_err(&self) -> bool {
		self.deref().is_err()
	}

	pub fn is_nil(&self) -> Result<bool, Val> {
		self.value().map(|v| v.is_nil())
	}

	pub fn len(&self) -> Result<usize, Val> {
		if let Some(len) = self.value()?.len() {
			Ok(len)
		} else {
			Err(crate::err::Err::new(format!("{:?} doesn't have a len.", self)))
		}
	}

	pub fn index(&self, k: Val) -> Val {
		let this = self.value()?;

		if let Ok(s) = k.get_str() {
			this.index_str(s)
		} else if let Ok(n) = k.get_int() {
			this.index_int(n as usize)
		} else {
			crate::err::Err::new(format!("Can't index with a {:?}", k))
		}
	}

	pub fn index_int(&self, k: usize) -> Val {
		self.value().unwrap().index_int(k)
	}

	pub fn index_str(&self, key: &str) -> Val {
		self.value().unwrap().index_str(key)
	}

	pub fn add(&self, that: Val) -> Val {
		self.value()?.add(&*that.value()?)
	}

	pub fn subtract(&self, that: Val) -> Val {
		self.value()?.subtract(&*that.value()?)
	}

	pub fn divide(&self, that: Val) -> Val {
		let l = self.value()?;
		let r = that.value()?;
		l.get_num()
			.and_then(|l| r.get_num().map(|r| (l, r)))
			.filter(|(_, r)| !num::Zero::is_zero(*r))
			.map(|(l, r)| crate::num::get(l / r))
			.unwrap_or_else(|| crate::err::Err::new(format!("Can't divide {:?} / {:?}", l, r)))
	}

	pub fn modulus(&self, that: Val) -> Val {
		let l = self.value()?;
		let r = that.value()?;
		l.get_num()
			.and_then(|l| r.get_num().map(|r| (l, r)))
			.filter(|(_, r)| !num::Zero::is_zero(*r))
			.map(|(l, r)| crate::num::get(l % r))
			.unwrap_or_else(|| crate::err::Err::new(format!("Can't divide {:?} % {:?}", l, r)))
	}

	pub fn multiply(&self, that: Val) -> Val {
		let l = self.value()?;
		let r = that.value()?;
		l.get_num()
			.and_then(|l|
				r.get_num().map(|r| crate::num::get(l * r))
			)
			.unwrap_or_else(|| crate::err::Err::new(format!("Can't multiply {:?} * {:?}", l, r)))
	}

	pub fn neg(&self) -> Val {
		self.value()?.neg()
	}

	pub fn eq(&self, that: crate::Val) -> Val {
		self.value()?.eq(&*that.value()?)
	}

	pub fn ne(&self, that: crate::Val) -> Val {
		self.eq(that).not()
	}

	pub fn cmp(&self, that: crate::Val) -> Result<std::cmp::Ordering,Val> {
		self.value()?.cmp(&*that.value()?)
	}

	pub fn call(&self, arg: Val) -> Val {
		self.value()?.call(arg)
	}

	pub fn get_str(&self) -> Result<&str,Val> {
		self.value()?
			.get_str()
			.map(|r| crate::i_promise_this_will_stay_alive(r))
			.ok_or_else(|| crate::Err::new(format!("{:?} can not be used as a string", self)))
	}

	pub fn to_string(&self) -> Val {
		let v = self.value()?;
		if v.type_str() == "string" { return self.clone() }
		v.to_string()
	}

	pub fn to_bool(&self) -> Result<bool, Val> {
		Ok(self.value()?.to_bool().get_bool().unwrap())
	}

	pub fn not(&self) -> Val {
		crate::bool::get(!self.to_bool()?)
	}

	pub fn iter<'a>(&'a self) -> Result<Box<dyn Iterator<Item=Val> + 'a>, Val> {
		let this = self.value()?;
		crate::i_promise_this_will_stay_alive(&*this)
			.iter()
			.ok_or_else(|| crate::Err::new(format!("Can't iterate over {:?}", this)))
	}

	pub fn iter_dict<'a>(&'a self) -> Result<Box<dyn Iterator<Item=(&'a str, Val)> + 'a>, Val> {
		let this = self.value()?;
		crate::i_promise_this_will_stay_alive(&*this)
			.iter_dict()
			.ok_or_else(|| crate::Err::new(format!("Can't iterate over {:?}", this)))
	}

	pub fn reverse_iter<'a>(&'a self) -> Result<Box<dyn Iterator<Item=Val> + 'a>, Val> {
		let this = self.value()?;
		crate::i_promise_this_will_stay_alive(&*this)
			.reverse_iter()
			.ok_or_else(|| crate::Err::new(format!("Can't reverse-iterate over {:?}", this)))
	}

	pub fn foldl(&self, f: Val, accum: Val) -> Val {
		self.iter()?.fold(accum, |accum, elem| f.call(accum).call(elem))
	}

	pub fn foldr(&self, f: Val, accum: Val) -> Val {
		self.reverse_iter()?.fold(accum, |accum, elem| f.call(accum).call(elem))
	}

	pub fn filter(&self, f: Val) -> Val {
		let pool = self.pool.clone().unwrap_or_else(crate::mem::Pool::new);
		let vals = self.iter()?
			.flat_map(|v| match f.call(v.clone()).to_bool() {
				Ok(true) => {
					Some(Ok(crate::thunk::shim(v.downgrade(&pool)).into()))
				}
				Ok(false) => None,
				Err(e) => Some(Err(e)),
			})
			.collect::<Result<Vec<_>,_>>();
		crate::list::List::of_vals(pool, vals?)
	}

	pub fn flatten(&self) -> Val {
		let pool = self.pool.clone().unwrap_or_else(crate::mem::Pool::new);
		let mut r = Vec::new();
		for v in self.iter()? {
			if let Some(iter) = v.value()?.iter() {
				r.extend(iter
					.map(|v| v.downgrade(&pool))
					.map(crate::thunk::shim));
				continue
			}
			r.push(crate::thunk::shim(v.downgrade(&pool)));
		}
		crate::list::List::of_vals(pool, r)
	}

	pub fn map(&self, f: Val) -> Val {
		fn do_map((f, v): (Val, Val)) -> Val {
			f.call(v)
		}

		let vals = self.iter()?
			.map(move |v| {
				crate::thunk::Thunk::new(
					(f.clone(), v.clone()),
					&do_map)
			})
			.collect();

		let pool = self.pool.clone().unwrap_or_else(crate::mem::Pool::new);
		crate::list::List::of_vals(pool, vals)
	}

	pub fn reverse(&self) -> Val {
		self.value()?.reverse()
	}

	pub fn eval(self) -> Result<Val,Val> {
		let v = self.deref();

		// Handle the error case here, because otherwise the error will have no
		// reference to the pool with which to clone itself.
		if v.is_err() {
			return Err(self)
		}
		v.eval().map(|()| self)
	}
}

impl std::fmt::Debug for Val {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		self.deref().fmt(f)
	}
}

impl std::fmt::Display for Val {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		self.deref().fmt(f)
	}
}

impl std::ops::FromResidual for Val {
	fn from_residual(residual: Val) -> Self { residual }
}

impl<E: Into<Val>> std::ops::FromResidual<Result<std::convert::Infallible, E>> for Val {
	fn from_residual(residual: Result<std::convert::Infallible, E>) -> Self {
		residual.unwrap_err().into()
	}
}

impl std::ops::Try for Val {
	type Output = Self;
	type Residual = Val;

	fn from_output(output: Self::Output) -> Self { output }
	fn branch(self) -> std::ops::ControlFlow<Self::Residual, Self::Output> {
		if self.is_err() {
			std::ops::ControlFlow::Break(self)
		} else {
			std::ops::ControlFlow::Continue(self)
		}
	}
}

fn unerase<E: serde::ser::Error>(e: impl std::fmt::Display) -> E {
	E::custom(format!("{}", e))
}

impl serde::Serialize for Val {
	fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
		let v = self.value().map_err(unerase)?;
		erased_serde::serialize(&v, s)
	}
}

impl std::error::Error for Val {
}

#[derive(Clone, Debug)]
pub enum WeakVal {
	Atomic(std::rc::Rc<dyn Value>),
	Bool(bool),
	Nil,
	Null,
	Ref(std::rc::Weak<dyn Value>),
}

impl WeakVal {
	fn deref(&self) -> &dyn Value {
		match self {
			WeakVal::Atomic(ref rc) => {
				crate::i_promise_this_will_stay_alive(&**rc)
			}
			WeakVal::Bool(ref b) => b,
			WeakVal::Nil => &crate::nil::Nil,
			WeakVal::Null => &crate::null::Null,
			WeakVal::Ref(ref weak) => {
				crate::i_promise_this_will_stay_alive(&*weak.upgrade().expect("ref upgrade"))
			}
		}
	}

	pub fn upgrade(&self) -> Val {
		Val {
			pool: self.deref()
				.pool()
				.map(|p| p.clone().upgrade()),
			weak: self.clone(),
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn val_is_rc_sized() {
		// Testing an implementation detail. A Inline should be two words. One
		// is for the target and one is for the vtable. Right now we also
		// have a discriminant which we should hide.
		// Val also contains a pool pointer.
		assert_eq!(
			std::mem::size_of::<WeakVal>(),
			3*std::mem::size_of::<*const bool>());
	}

	#[test]
	fn list() {
		assert_eq!(crate::eval("<str>", "[]").is_empty().get_bool(), Some(true));
		let v = crate::eval("<str>", "[0d29 0b1.1]");
		assert_eq!(v.is_empty().get_bool(), Some(false));
		assert_eq!(v.index_int(0).get_int().unwrap(), 29);
		assert_eq!(v.index_int(1).as_f64().unwrap(), 1.5);
	}

	#[test]
	fn ident() {
		assert_eq!(crate::eval("<str>","{b = 4}.b").get_int().unwrap(), 4);
	}

	#[test]
	fn recursion() {
		let v = crate::eval("<str>" ,"{b = b}.b + 5").eval().unwrap_err();
		let s = format!("{:?}", v);
		assert!(s.contains("Dependency cycle detected."), "In: {:?}", s);
	}

	#[test]
	fn recursion_multi_step() {
		let v = crate::eval("<str>", "{
			a = b
			b = c
			c = d
			d = b
		}.a").eval().unwrap_err();
		let s = format!("{:?}", v);
		assert!(s.contains("Dependency cycle detected."), "In: {:?}", s);
	}
}
