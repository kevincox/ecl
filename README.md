# ecl - Expressive Config Language

ecl is a language designed for creating maintainable configuration, even when the domain is complex. It provides a powerful, yet understandable to let you avoid repetition and express yourself concisely.

[Reference Documentation](doc/index.md)

## Key Beliefs

- **Obvious**: ecl does what you expect, and be simple to debug when something goes wrong.
- **Expressive**: Configuration gets complex. Instead of hiding from the problem, which often leads to repetition or hacky preprocessing, ecl acknowledges this and provides you the tool you need, while encouraging simplicity.
- **Pure**: ecl evaluates to a simple value. This allows easy verifying your made the changes that you intended. Simply running a diff of the output will show all changes. This also provides referential transparency, this makes it easy to create reusable modules.
- **Standalone**: ecl isn't part of any program or system. It's intended use is to be compiled into a POD format (like JSON or Protocol Buffers) before being used. This gives you an easy introspection point and allows ecl to be used for a wide variety of uses.

## Other features.

- ecl is lazy. This means that you can store multiple related configurations in the same file. You can then easily evaluate them separately.
- Multiple output formats. While you may wish to use something fast like Protocol Buffers in production it is easy to generate other formats that are easier for diffing and debugging.
- Testing. ecl (soon) supports assertions in the configuration to file errors early.

## Plans for new lookup rules and extension rules.

### Simple dict

```ecl
# webapp.ecl
time-zone = "UTC"

database-host = "localhost:1234"
database-name = "webapp"

log-dir = "data/logs"
image-dir = "data/images"

log-level = "warn"
"500-debug-info" = false # If 500 responses should show debugging information.

sign-cookies = true
require-signed-cookies = true
```

# Complex

```ecl
local config = ->{env} {
	time-zone = cond:[
		env == "dev" "localtime"
		"UTC"]

	local database-host = {
		prod = "prd-db-0"
		dev = "localhost"
	}."{env}"
	local database-port = 1234
	assert 0 < database-port < 64Ki

	database-addr = "${database-host}:${database-port}"

	base-dir = "data"
	log-dir = "{base-dir}/logs"
	image-dir = "{base-dir}/images"

	log-level = cond:[
		env == "dev" "warn"
		"info"]
}

prod = config:{env="prod"}
dev = config:{env="dev"}
```

## Development

### Fuzzing

```sh
nix-shell -A fuzz --run 'cargo fuzz run eval'
```
