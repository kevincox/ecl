# ECL

ECL is a language designed for maintainable, understandable configuration.

Notable features:
- Functional: ECL doesn't support value mutation of any kind.
- Lazy: ECL only computes the values required to return the result.
- Pure: ECL makes it easy to make repeatable computation by making most operations consistent and limiting access to sources of randomness.

[[_TOC_]]

## Types

### nil

`nil` is a type with a single value. `nil` is equal only to itself, and is considered falsey. You can access the `nil` value via the global of the same name.

```ecl
assert nil == nil # Equals itself
assert nil != false # Doesn't equal anything else.
assert !nil # Falsey

assert type:nil == "nil"
```

### bool

There are two `bool` values, `true` and `false` accessible by the globals of the same name.

```ecl
assert true != false
assert !true == false
assert true == !false

assert true
assert !false

assert type:true == type:false == "bool"
```

#### Truthiness

All[^except-error] values can be converted to `bool` values. For this conversion `false` `nil` and `null` convert to `false`. All other values[^except-error] convert to `true`.

> Note: Attempting to convert an `error` to `bool` will result in an `error`, not `true`.

```ecl
assert !nil
assert true
assert !false
assert 0
assert 1
assert ""
assert "false"
assert []
assert [nil]
assert {}
assert {a=1}
```

### number

A `number` represents a [rational number](https://en.wikipedia.org/wiki/Rational_number) of arbitrary precision. There are many ways to write rational numbers.

```ecl
assert 12345
assert 0.5 == 0b0.1
assert 1Mi == 0x100000
assert 1M == 1_000_000

assert type:1e42 == "number"
```

### string

A `string` represents an arbitrary sequence of [Unicode Scalar Values](https://unicode.org/glossary/#unicode_scalar_value).

```ecl
assert "Hello 👋 " + "🌎" == "Hello 👋 🌎"

assert type:" " == "string"
```

> Note: ECL currently has no "bytes" type. For binary data you should use an array of integers.

#### Escape Sequences

Most characters can be included directly into an ECL string, however a couple of values are restricted. You can escape any character outside of `A-Za-z0-9` with a backslash to ensure that it is included literally in the string. Some letters and numbers have a special meaning as described below, other letters and numbers are reserved for future use and will raise an error.

- `\n`: A newline character. (`U+000A`)
- `\t`: A horizontal tab character. (`U+0007`)
- `\0`: A null byte. (`U+0000`)

#### Nested Strings

To avoid escaping a lot of quotes in string literals you may use 3 or more quotes to start the string literal. You will then need that many quotes to close the string literal.

```ecl
assert """
	She said "Hello".
""" == "She said \"Hello\".\n"
```

#### String Interpolation

You can interpolate dynamic values into string literals. Any expressions inside `{}` will be converted to strings and included in the string.

```ecl
local num = 5

assert "node-{num}" == "node-5"
assert "node-{num + 2}" == "node-7"

# Use a backslash to escape an interpolation.
assert "node-\{num}" == "node-\{n" + "um}"
```

### function

A `function` is a value that can be called. All functions take a single argument. For operations that require more than one argument passing a `dict`, `list` or [currying](https://en.wikipedia.org/wiki/Currying) can be used.

You call a function using the `:` operator.

```ecl
assert type:reverse == "function"
assert reverse:[1 2 3] == [3 2 1]

# Using a curried argument:
assert map:reverse:[[1 2 3] ["a" "b" "c"]] == [[3 2 1] ["c" "b" "a"]]

# Using a dict for keyword arguments:
assert pad:{to=5 with="*"}:"Hi" == "***Hi"

# Using a list for multiple arguments:
local n = false
assert cond:[n == nil "is nil" "not nil"] == "not nil"
```

#### User Defined Functions

You can define a function using the `->` operator. After the arrow comes the argument definition, then the body.

```ecl
local f = ->name "Hello {name}"
assert f:"Dipper" == "Hello Dipper"

local add = ->a ->b a + b
assert add:1:2 == 3

# list destructuring arguments:
local try-env = ->[var else=""] {
	value = env:var
	r = cond:[value value else]
}.r
assert try-env:["HOME"] != ""
assert try-env:["UNSET" "5"] == "5"

# dict destructuring arguments:
local name-builder = ->{honorific first middle="" last} "{honorific} {first} {middle} {last}"
assert name-builder:{honorific="Mr." first="Stanford" last="Pines"} == "Mr. Stanford  Pines"
```

### list

A `list` is an ordered collection of heterogeneous elements.

```ecl
assert [nil true 5] != [5 nil true]

assert type:[] == "list"
```

### dict

A `dict` is an unordered collection of unique `string` keys and an associated value for each key. The associated values need not be unique and may be heterogeneous and of any type.

```ecl
assert {a=1 b=true} == {b=true a=1}

assert type:{} == "dict"
```

#### Key Quoting

You need to quote dictionary keys if they are not valid identifiers.

```ecl
assert {"🤓👈"=true}."🤓👈"
```

#### Dict Value References

Inside a dictionary literal you can reference other keys by name. (as long as they are valid identifiers)

```ecl
profit = {
	revenue = 100
	costs = 10
	profit = revenue - costs
}.profit
```

#### assert

Dictionary literals can contain `assert` statements to validate the values.

```ecl
local db-uri = ->{host port} {
	assert 0 < port < 64Ki
	uri = "postgresql://{host}:{port}"
}.uri

primary = db-uri:{host="db-primary" port=5432}
assert error-msg:(db-uri:{host="db-primary" port=67890}) == "Assertion failed"
```

Assertions will be evaluated if the `dict` is evaluated. Note that this means that assertions for unevaluated `dict`s won't trigger. This can be useful for inheritance.

```ecl
local base = {
	name = error: "Name must be set."
	age = error: "Age must be set."
	assert age >= 18

	likes-cake = true
}
assert type:(base.likes-cake) == "error"

suzy = base:{name="Suzy" age=24}
```

This example evaluates successfully even though `base` itself evaluates to an error. This is because locals are not evaluated unless required and the call operator doesn't evaluate its arguments. Only `suzy` is evaluated and the assertion is satisfied in that case.

#### Inheritance

In ECL `dict`s can inherit from each other. This is similar to creating a subclass in Java or C++. You can think of each key in the `dict` as a virtual method.

```ecl
local database = {
	type = error: "type required"

	host = error: "host required"
	port = error: "port required"
	assert 0 < port < 64Ki

	uri = "{type}://{host}:{port}"
}

local postgresql-database = database:{
	type = "postgresql"
	port = 5432
}

assert postgresql-database:{host="prod-db-primary.internal"} == {
	type = "postgresql"
	host = "prod-db-primary.internal"
	port = 5432
	uri = "postgresql://prod-db-primary.internal:5432"
}
```

Note that `assert` does not have any special behaviour with regards to inheritance. However typically the "pure virtual" bases are not evaluated so the assert will not trigger. This is one of the reasons that this example uses a `local` for the base `database` and `postgres-database` `dict`s.

### error

`error` is a type in ECL that is quite unusual compared to the other types. Most operations on an `error` value will result in the `error` propagating (with additional trace info added for debugging). It is unlikely that you need to explicitly work with `error` values.

> Note: When this document says "all values" it is excluding `error` unless explicitly specified.

```sh
$ ecl eval '(error:"Test Message" + 3 && true)'
Error serializing: At line 1.27 On left side of &&
At line 1.23 On left side of addition
At line 1.7 returned from call
Calling builtin error.
Test Message
```

Note that there are a couple of operations that can take an `error` and evaluate to a non-`error` value such as `type` and `error-msg`. Use of these functions is heavily discouraged as it can lead to difficult to debug scenarios.

```ecl
local template = ->options {
	# DO NOT DO THIS: This is the wrong way to have an optional parameter.
	# The intent is to use a default value of 8 if nothing is specified, however by catching errors we can hide mistakes.
	cores = cond:[type:options.cores == "error" 8 options.cores]
}

facts = {
	core-count = 128
}

# Oops, the config still evaluates and our mistake will be hard to find.
config = template:{cores=facts.cre-count}
```

## Operators

A variety of operators are supported. ECL has very simple precedence rules so that it is easy to remember how the operators fit together. However for readability it is still a good idea to use parentheses whenever it is not completely obvious.

### Operator Precedence

The operators are listed lowest to highest. Operators inside a precedence group are evaluated left-to-right.

- `&&` `||`
- `==` `!=` `>=` `<=` `>` `<`
- `:` `+` `-` `*` `/` `%`
- Unary `-` `!`

### `&&` (and)

`&&` evaluates to the left value if it is [falsey](#truthiness), otherwise it evaluates to the right value.

Note that if the left value is falsey then it will be returned **even if** the right value is an error.

```ecl
assert ("Hello" && "World") == "World"
assert (nil && "World") == nil
assert (true && 5 && [] && false) == false

assert !(false && error:"Not evaluated")
```

### `||` (or)

`||` evaluates to the left value if it is [truthy](#truthiness), otherwise it evaluates to the right value.

Note that if the left value is truthy then it will be returned **even if** the right value is an error.

```ecl
assert ("Hello" || "World") == "Hello"
assert (nil || "World") == "World"
assert (nil || false || null || nil || 5) == 5

assert true || error:"Not evaluated"
```

### `==` `!=` `>=` `<=` `>` `<` (comparison operators)

The above operators are used for comparing values of the same type. These operators can be chained to make multiple comparisons easier to read. In a chain the value to the left and right of each operator is compared. AKA `A < B <= C == A < B && B <= C` for any values and any operators.

`==` and `!=` will always be `false` for values of the same type (unless one of the values is `error`). The other operators will evaluate to an `error` if the argument types are different.

```ecl
assert 4 > 2
assert "apple" < "plum"
assert false < true

assert -3 < -1 <= -1 <= 2 == 2 < 10

# lists are compared lexographically.
assert [1 2 3] < [3 2 1]
assert [1 2 3] < [1 3]

# dicts are compared lexographically by pair.
assert {a=1} < {c=1} # ["a" 1] < ["c" 1]
assert {a=1} < {a=2} # ["a" 1] < ["a" 2]

assert error-msg:("0" < 2) == """Can't compare "0" and 2"""

assert 4 != nil
```

### `:` (call)

The binary `:` operator is used to [call functions](#function) and [inherit from `dict`s](#inheritance).

```ecl
assert reverse:[1 2 3] == [3 2 1]
assert {a=1 b=a+1}:{a=3 c=5} == {a=3 b=4 c=5}
```

### `*` (multiply)

```ecl
assert 4 * 7 == 28
```

### `/` (divide)

```ecl
assert 4/2 == 2
assert 7/2 == 3.5
```

### `%` (remainder)

```ecl
assert 7 % 2 == 1
```

### `+` (plus)

The binary `+` operator is used to "add" or "join" two values of the same type.

```ecl
assert 1 + 2 == 3
assert "Hello" + "World" == "HelloWorld"
assert [1 2 3] + ["four" "five"] == [1 2 3 "four" "five"]
```

### `-` (minus)

#### Binary Minus

The binary `-` operator is used to "subtract" two numbers.

> Warning: The `-` operator must be separated from the end of an identifier by whitespace. Otherwise it will be considered part of the identifier.

```ecl
assert 5 - 3 == 2

local foo = 1
local bar = 2
local foo-bar = 3
assert foo - bar == -1
assert foo-bar == 3
```

#### Unary Minus

The unary `-` operator is used to negate a number.

> Warning: The binary `-` has precedence over unary `-`. This matters in a list literal.

```ecl
local bar = 5
assert -bar == -5

assert [3 -1] == [2]
```

### `!` (not)

`!` returns `true` if the argument is `falsey` and `false` if it is `truthy`[^except-error].

```ecl
assert !false
assert !nil
assert !!"Hello"
```

## Globals

### cond

> cond:<list>

`cond` takes any number of condition-value pairs, followed by an optional "else" value.

```ecl
assert cond:[true "a" "b"] == "a"
assert cond:[false "a" "b"] == "b"

assert cond:[
	false "a"
	false "b"
	false "c"
	false "d"
	true "e"
	false "f"
	true "g"
] == "e"

local age = 47
local can-see = true
assert cond:[
	(age < 16) "Too young"
	(age > 120) "Too old"
	(!can-see) "Not safe"
	"You can drive!"
] == "You can drive!"
```

### env

> env:<string>

Evaluates to the value of the environment variable named by the argument as a `string` or `nil` if it isn't set.

```ecl
assert env:"NOT_SET" == nil
assert env:"HOME" != nil
```

### error

> error:<string>

Creates an error value with the specified message.

```ecl
assert type:(error:"A message") == "error"
assert error-msg:(error:"A message") == "A message"

local base = {
	id = error: "id is required to be overriden by subclasses."
}
```

### error-msg

> error-msg:error

> WARNING: Avoid using this function, it can lead to hard-to-debug undesired behaviour.

Returns the error message associated with the passed error.

```ecl
assert error-msg:{}.foo == """No Pub("foo") in \{\}"""
```

### eval

> eval:<string>

> WARNING: Avoid using this function, it can lead to unmaintainable code.

Evaluate the given string as ECL code. The code is evaluated in its own top-level context so it can not access any variables of the surrounding code.

> Note: The code is evaluated as a full ECL document. If you want to evaluate only an expression wrap it in parentheses (`()`).

```ecl
assert eval:"(1 + 3)" == 4

local v = true
assert error-msg:(eval:"(v)") == """1.2 Invalid reference "v""""

assert (eval:"\{a=1 b=a+1}"):{a=4} == {a=4 b=5}
```

### hex

> hex:<number>

Print the hex representation of a value.

```ecl
assert hex:0 == "0"
assert hex:43 == "2b"

assert pad:{to=8 with=0}:(hex:0xab12) == "0000ab12"
```

### index

> index:<key any>:<value any>

Use `key` to index `value`.

```ecl
assert index:"a":{a=1} == 1
assert index:1:["zero" "one" "two"] == "one"
```

### false

> false

The [`bool`](#bool) value `false`.

### filter

> filter:<function>:<iterable>

Return a new `list` of the values from the `iterable` where the `function` returned true.

```ecl
assert filter:(->i i < 10):[12 3 10 2 192] == [3 2]
assert filter:(->[k v] v < 10):{a=12 b=3 c=-12} == [["b" 3] ["c" (-12)]]
```

### flatten

> flatten:<iterable>

Return a `list` with one level of nesting removed.

```ecl
assert flatten:[[1] [] [2 [3]]] == [1 2 [3]]
assert flatten:{a=1 b=[2] c={}} == ["a" 1 "b" [2] "c" {}]
assert flatten:[{a=1} {b=2}] == [["a" 1] ["b" 2]]
```

### foldl

> foldl:(->accumulator ->element ...):<initial any>:<iterable>

Fold over the iterable from left to right.

```ecl
assert foldl:(->a ->e a + e):"":["1" "2" "3"] == "123"
```

### foldr

> foldr:(->accumulator ->element ...):<initial any>:<iterable>

Fold over the iterable from right to left.

```ecl
assert foldr:(->a ->e a + e):"":["1" "2" "3"] == "321"
```

### load

> load:<string>

Load the ECL file from the given path.

```ecl
# Example broken because the doc is tested using `eval`.
# assert (load:./data.ecl).cores == 16
```

### map

> map:<function>:<iterable>

Create a list by transforming every element of `iterable` using `function`.

```ecl
assert map:->s "green {s}":["apple" "tea" "thumb"] == ["green apple" "green tea" "green thumb"]
```

### nil

> nil

The value [`nil`](#nil).

### null

The value `null`. `null` is similar to [`nil`](#nil) except that it is returned as a value in the output.

```ecl
assert null != nil
```

### pad

> pad:{to=<number>, ...}:<string>

Pad a string to a given length. The first argument is a configuration object which can have the following arguments.

The argument `to` is required. Other arguments are shown with their default value.

- `to`: The length to pad to.
- `align="right"`: How to align the input string in the result:
	- `"left"`: Add padding to the right.
	- `"right"`: Add padding to the left.
	- `"center-left"`: Add padding on both sides, breaking ties by adding extra to the right.
	- `"center-right"`: Add padding on both sides, breaking ties by adding extra to the left.
- `uneven="error"`: What do to if the `with` parameter is multiple characters and doesn't evenly fill the desired width.
	- `"error"` raise an error.
	- `"over"` Pad the string so that it is **at least** `to` characters long.
	- `"under"` Pad the string so that it is **at most** `to` characters long.
- `with=" "`: The string to pad with.

```ecl
assert pad:{to=5}:"cat" == "  cat"
```

### read

> read:<string>

Read the given file into a string.

```ecl
assert read:"/dev/null" == ""
```

### replace

> replace:<dict>:<string>

Perform a search and replace on the given string. The first argument is a configuration object which can have the following arguments.

One of `regex` or `text` are required. Other arguments are shown with their default value.

- `regex`: A regex to search for.
- `text`: A fixed string to search for.
- `case=true`: If searching should be case-sensitive.
- `limit=0`: If non-zero replace at most this many matches.
- `multiline=true`: If `true` `^` matches the beginning of the string and `$` matches the end of the string instead of the beginning and end of lines.
- `multiline-dot=nil`:
	- `true`: `.` in `regex` can match newline characters (`"\n"`).
	- `false`: `.` in `regex` matches any character except a newline.
	- `nil`: The value of `multiline` is used.
- `with=""`: The replacement string. If `with` contains `$0` it will be replaced by the matched text, if it contains `${n}` it will be replaced with the text matched by the `{n}`th subgroup.

```ecl
assert replace:{text="cat"}:"The cat left" == "The  left"
assert replace:{regex="\\d" with="$"}:"We made $10,200 last year" == "We made $$$,$$$ last year"
```

### reverse

> reverse:<iterable>

Evaluates to a `list` with the same elements in the opposite order of `iterable`.

```ecl
assert reverse:[1 2 3] == [3 2 1]
```

### sha1

> sha1:<string>

Evaluates to the [SHA-1](https://en.wikipedia.org/wiki/SHA-1) hash of `string` as a `number`.

```ecl
assert sha1:"sha1" == 0x415ab40ae9b7cc4e66d6769cb2c08106e8293b48
assert pad:{to=40 with=0}:(hex:(sha1:"leading-zero")) == "03de018621a2f546d613002d6425fb82324cef50"
```

### slice

> slice:<dict>:<list>

Evaluate to a subset of elements from the list. The first argument can contain the following arguments:

- `from=0`: The first element to include.
- `count=nil`: If set the number of elements to include. If `nil` `to=` is used.
- `to=nil`: The last element to include.
	- `nil`: Include elements until the end of the list.
	- `>=0`: Include element until the specified index.
	- `<0`: Include elements until the given element from the end. For example `-2` will include all but the last 2 elements.

```ecl
assert slice:{from=2}:[0 1 2 3 4] == [2 3 4]
assert slice:{to=2}:[0 1 2 3 4] == [0 1]
assert slice:{to=-3}:[0 1 2 3 4] == [0 1]
assert slice:{from=-3 count=2}:[0 1 2 3 4] == [2 3]
```

### split

> split:<dict>:<string>

Split a string into multiple strings by a given pattern.

Arguments are shown with their default value.

- `regex="\\s+"`: A regex to search for.
- `text`: A fixed string to search for.
- `case=true`: If searching should be case-sensitive.
- `limit=0`: If non-zero at most that many strings will be returned. The last string will contain the remainder of the string even if there are more matches..
- `multiline=true`: If `true` `^` matches the beginning of the string and `$` matches the end of the string instead of the beginning and end of lines.
- `multiline-dot=nil`:
	- `true`: `.` in `regex` can match newline characters (`"\n"`).
	- `false`: `.` in `regex` matches any character except a newline.
	- `nil`: The value of `multiline` is used.

```ecl
assert split:{}:"""
	This is a
		sad story.
""" == ["This" "is" "a" "sad" "story." ""]
```

### true

> true

The [`bool`](#bool) value `true`.

### type

> type:<any>

Returns the type of the argument as a string.

> WARNING: This function can hide `error` values as `type:<error> == "error"`. Be cautious that you don't lose errors this way as it can lead to unexpected and hard to debug results.

```ecl
assert type:1 == "number"
assert type:"foo" == "string"
```

[^except-error]: except error values, which cause the expression to evaluate to that error.
