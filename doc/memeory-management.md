# Memory Management

Memory management in ECL is done via memory pools. Due to the immutable nature of ECL we can use a form of reference counting to reclaim temporary memory from computations while avoiding worrying about subgraphs becoming unreachable.

For example imagine nested dicts.

```ecl
top = {
	a = "foo"
	b = {bar=5}
}
```

When evaluating we first create a Pool for the outer dict. This holds the string `"foo"`. However when we evaluate `b` we first create a new Pool. Once the evaluation is complete and we detect that the subdict will be a member of the outer dict we merge the pools together. This is because the everything reachable from the outer dict is reachable from the subdict and vice versa.

```
outer-pool = ["foo" {...}]
inner-pool = { parent = outer-pool }
```

The tricky park is getting reference counting working properly. Do do this we use weak references for basically all internal references. The pool will then have strong references to the values inside of it, and the top-level owner will maintain a strong reference to the pool. This means that when the top-level owner drops their reference the Pool will deallocate everything that it holds.

Note that despite the fact that we use weak references we never actually expect to experience a failure to upgrade the reference. Everything that we ever want to upgrade should be live. In fact you can consider the weak references to just be a runtime safety check. Maybe someday we will null out this reference counting for production builds.

## Pool Merging

Merging pools is surprisingly weird. This is because once a pool is merged a reference to either of the original pools still needs to be able to find the current pool location. This can be to allocate more objects into it (for example a lazy-evaluated dict) or to merge again.

In order to make this work pools are referenced via a tree of reference counted links. When a pool is merged the old "root" is replaced with a reference to the new pool location.

As an optimization whenever a pool reference is used it will update its nearest link to point directly at the root. This should avoid continuously chasing a long tail of links.
