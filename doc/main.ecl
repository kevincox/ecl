local split-doc = split:{regex="\n```ecl\n"}
local split-code = replace:{regex="\n```\n.*"}

local test-markdown = ->file {
	local content = read:file
	local blocks = split-doc: content
	local code-blocks = slice:{from=1}:blocks
	local code = map:split-code:code-blocks
	result = map:eval:code
}

test-readme = test-markdown:../README.md
test-docs = test-markdown:./index.md
