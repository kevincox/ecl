mod utils;

use serde::Serialize;

fn main() {
	utils::test_dir("lines", "ecl", |path| {
		let mut output = Vec::new();
		ecl::eval_file(&path.to_string_lossy())
			.serialize(&mut ecl::lines::Serializer::new(&mut output)).unwrap();

		utils::diff(&output, &path.with_extension("lines"));
	})
}
