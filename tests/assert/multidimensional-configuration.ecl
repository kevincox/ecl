local config = ->{env country instance} {
	local full-instance = "{env}-{country}-{instance}"

	hostname = {
		prod = "api.company.example"
		qa = "beta.api.company.example"
		dev = "dev-api.internal"
	}."{env}"

	logging-endpoint = cond:[
		env == "dev" "https://beta.logging.example"
		{
			us = "https://us.logging.example"
			de = "https://eu.logging.example"
		}."{country}"
	]

	tasks = cond:[
		env != "prod" 3
		{
			prod-de-central = 41
			prod-us-east = 28
			prod-us-west = 50
		}."{full-instance}"
	]

	local database-info = {
		prod = {
			primary = "postgresql://api:passw0rd@prod-db-primary.internal"
			replica = "postgresql://api:passw0rd@prod-db-replica.internal"
		}
		qa = prod
		dev = {
			primary = "postgresql://api:passw0rd@dev-db-primary.internal"
			replica = primary
		}
	}."{env}"

	database-uri = database-info.primary
	database-replica-uri = database-info.replica

	threadpool-size = 8
}


assert config:{env="prod" country="de" instance="central"} == {
	hostname = "api.company.example"
	logging-endpoint = "https://eu.logging.example"
	tasks = 41

	database-uri = "postgresql://api:passw0rd@prod-db-primary.internal"
	database-replica-uri = "postgresql://api:passw0rd@prod-db-replica.internal"

	threadpool-size = 8
}

assert config:{env="prod" country="us" instance="east"} == {
	hostname = "api.company.example"
	logging-endpoint = "https://us.logging.example"
	tasks = 28

	database-uri = "postgresql://api:passw0rd@prod-db-primary.internal"
	database-replica-uri = "postgresql://api:passw0rd@prod-db-replica.internal"

	threadpool-size = 8
}

assert config:{env="prod" country="us" instance="west"} == {
	hostname = "api.company.example"
	logging-endpoint = "https://us.logging.example"
	tasks = 50

	database-uri = "postgresql://api:passw0rd@prod-db-primary.internal"
	database-replica-uri = "postgresql://api:passw0rd@prod-db-replica.internal"

	threadpool-size = 8
}

assert config:{env="qa" country="de" instance="central"} == {
	hostname = "beta.api.company.example"
	logging-endpoint = "https://eu.logging.example"
	tasks = 3

	database-uri = "postgresql://api:passw0rd@prod-db-primary.internal"
	database-replica-uri = "postgresql://api:passw0rd@prod-db-replica.internal"

	threadpool-size = 8
}

assert config:{env="qa" country="us" instance="east"} == {
	hostname = "beta.api.company.example"
	logging-endpoint = "https://us.logging.example"
	tasks = 3

	database-uri = "postgresql://api:passw0rd@prod-db-primary.internal"
	database-replica-uri = "postgresql://api:passw0rd@prod-db-replica.internal"

	threadpool-size = 8
}

assert config:{env="dev" country="us" instance="east"} == {
	hostname = "dev-api.internal"
	logging-endpoint = "https://beta.logging.example"
	tasks = 3

	database-uri = "postgresql://api:passw0rd@dev-db-primary.internal"
	database-replica-uri = "postgresql://api:passw0rd@dev-db-primary.internal"

	threadpool-size = 8
}
