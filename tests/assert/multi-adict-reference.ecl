target = 5

source.other = true
source.this = {
	val = target
}

assert source.this.val == 5
