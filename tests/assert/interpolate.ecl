# An error in a string interpolation must not mess up interpreter state.
assert type:("{error: "example"} foo {4} {2}" < "a" < "b") == "error"
