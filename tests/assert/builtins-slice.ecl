assert slice:{from=3 to=5}:[0 1 2 3 4 5 6] == [3 4]
assert slice:{from=1 count=3}:[0 1 2 3 4 5 6] == [1 2 3]
assert slice:{from=1 to=-2}:[0 1 2 3 4 5 6] == [1 2 3 4]

assert error-msg:(slice:{from=4}:[0 1 2]) == "slice:\{from=4} is longer than 3 elements in [<unevaluated>, <unevaluated>, <unevaluated>]"
assert error-msg:(slice:{from=1 to=4}:[0 1 2]) == "slice:\{to=4} is longer than 3 elements in [<unevaluated>, <unevaluated>, <unevaluated>]"
assert error-msg:(slice:{to=-4}:[0 1 2]) == "slice:\{to=-4} is longer than 3 elements in [<unevaluated>, <unevaluated>, <unevaluated>]"
assert error-msg:(slice:{from=1 count=3}:[0 1 2]) == "slice:\{from=1 count=3} is longer than 3 elements in [<unevaluated>, <unevaluated>, <unevaluated>]"
