assert 1 == 1
assert 1 != "1"
assert "1" != 1
assert 1 != 2
assert "foo" != nil
assert nil != "foo"
assert nil == nil

assert type:(error:"foo" < 1) == "error"
assert type:(1 == error:"foo") == "error"
assert type:(0 < error:"foo" <= 4) == "error"
assert type:(0 < 1 < error:"foo") == "error"

assert (0 > 1 < error:"foo") == false
