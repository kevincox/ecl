assert replace:{regex="\\d+" with="($0)"}:"Alice bought 3 eggs for $14."
	== "Alice bought (3) eggs for $(14)."

assert replace:{regex="c.t"}:"A cat." == "A ."
assert replace:{text="c.t"}:"A cat." == "A cat."
assert replace:{text="cat"}:"A cat." == "A ."

assert replace:{regex="."}:"abc" == ""
assert replace:{regex="." limit=2}:"abc" == "c"

assert replace:{text="A"}:"a" == "a"
assert replace:{text="A" case=false}:"a" == ""

assert replace:{regex="." multiline=true}:"\n" == ""
assert replace:{regex="." multiline=false}:"\n" == "\n"
assert replace:{regex="^.*$" multiline=false}:"a\nb\n" == "\n\n"
