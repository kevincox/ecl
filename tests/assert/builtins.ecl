assert error-msg:(error: "Some message") == "Some message"

assert error-msg:(error-msg:nil) == "Non-error passed to error-msg:Nil"
assert error-msg:(error-msg:"str") == "Non-error passed to error-msg:\"str\""

assert eval:"(true)" == true
assert eval:"\{a=5}" == {a=5}

assert type:(error: "Test") == "error"
assert type:{} == "dict"
assert type:[] == "list"
assert type:nil == "nil"
assert type:10 == "number"
assert type:10.2 == "number"
assert type:"10.2" == "string"

assert error-msg:(map - env) == """Can't subtract <builtin "map"> and <builtin "env">"""
