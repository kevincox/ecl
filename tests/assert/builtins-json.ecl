assert to-json:"hello" == "\"hello\""
assert to-json:nil == "null"
assert to-json:5 == "5"
assert to-json:[1 true "" (-1)] == """[1,true,"",-1]"""
assert to-json:{
	dict = {
		bool = true
	}
	num = 5
	ref = dict
	list = [false]
} == """\{"dict":\{"bool":true},"list":[false],"num":5,"ref":\{"bool":true}}"""

assert type:(to-json:{err = error:"foo"}) == "error"
