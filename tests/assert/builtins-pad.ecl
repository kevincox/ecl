assert error-msg:(pad:{}) == "Missing required argument pad:\{to=<num>}."
assert error-msg:(pad:{ot=4}) == "Unknown option for pad:\{ot=4}."

assert pad:{to=0}:"" == ""
assert pad:{to=0}:"foo" == "foo"
assert pad:{to=4}:"" == "    "
assert pad:{to=4}:"hi" == "  hi"

assert pad:{to=6 with="hi"}:"this" == "hithis"

assert error-msg:(pad:{to=6 with="hi"}:"and") ==
	"Cannot pad string, no multiple of length 2 padding will fill the required 1 chars."
assert error-msg:(pad:{to=6 with="hi" uneven="error"}:"and") ==
	"Cannot pad string, no multiple of length 2 padding will fill the required 1 chars."
assert pad:{to=6 with="hi" uneven="over"}:"and" == "hihiand"
assert pad:{to=6 with="hi" uneven="under"}:"and" == "hiand"

assert pad:{to=4}:"|" == "   |"
assert pad:{to=4 align="right"}:"|" == "   |"
assert pad:{to=4 align="left"}:"|" == "|   "
assert pad:{to=4 align="center-right"}:"|" == "  | "
assert pad:{to=4 align="center-left"}:"|" == " |  "
assert pad:{to=5 align="center-right"}:"|" == "  |  "
assert pad:{to=5 align="center-left"}:"|" == "  |  "
