assert map:(->[k v] "--{k}={v}"):{foo=5 bar="baz"} == ["--bar=baz" "--foo=5"]
assert map:(replace:{regex="-.*"}):["foo-bar"] == ["foo"]
