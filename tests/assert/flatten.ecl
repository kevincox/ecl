assert flatten:[1 [2] [[3]]] == [1 2 [3]]
assert flatten:{a=true b=nil} == ["a" true "b" nil]
assert flatten:[{a=1 b=2} [3] {d=4}] == [["a" 1] ["b" 2] 3 ["d" 4]]

assert type:(flatten:[[1] error:"e" [3]]) == "error"
assert map:type:(flatten:[[1] [error:"e"] [nil]]) == ["number" "error" "nil"]
