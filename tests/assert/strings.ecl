assert "multi\nline\nstring\n" == """
	multi
	line
	string
"""

assert "multi\n\tindent\nstring\n" == """
	multi
		indent
	string
"""

# Test that "" is the empty string, not the opening of a ""two quote string""
assert "" + "" == ""

assert """Kevin "K-Dawg" Cox""" == "Kevin \"K-Dawg\" Cox"
assert """""In"""cept"""ion""""" == "In\"\"\"cept\"\"\"ion"

assert """Jim said: "Something"""" == "Jim said: \"Something\""
