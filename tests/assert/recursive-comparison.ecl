local x = {
	parent = x
}
local y = {
	parent = y
}
# assert x == x # Should this work? What if it contains an error? should x == x in general?
assert error-msg:(x == y) == "Dependency cycle detected."

foo = {
	thing = 5
	ref = thing
}
bar = {
	thing = 2 + 3
	ref = bar.thing
}
assert foo == bar
