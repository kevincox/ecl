# Check that the stringification of the recursive structures for the error message doesn't recursive forever.

local dict-parent = {rec=dict-parent}
assert type:(dict-parent.rec + 0) == "error"

local list-parent = [list-parent]
assert type:(index:list-parent:0 + 0) == "error"
