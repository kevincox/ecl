assert split:{}:"abc" == ["abc"]
assert split:{}:"a b c" == ["a" "b" "c"]
assert split:{}:"a \tb\nc" == ["a" "b" "c"]

assert split:{text="-"}:"foo-bar-baz" == ["foo" "bar" "baz"]
assert split:{regex="\\w+"}:" foo \nbar\tbaz" == [" " " \n" "\t" ""]

assert split:{text="" limit=3}:"watermelon" == ["w" "a" "termelon"]
