assert sha1:"sha1" == 0x415ab40ae9b7cc4e66d6769cb2c08106e8293b48
assert hex:(sha1:"sha1") == "415ab40ae9b7cc4e66d6769cb2c08106e8293b48"
assert hex:(sha1:"leading-zero") == "3de018621a2f546d613002d6425fb82324cef50"
assert pad:{to=40 with=0}:(hex:(sha1:"leading-zero")) == "03de018621a2f546d613002d6425fb82324cef50"
