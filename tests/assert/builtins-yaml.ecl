assert to-yaml:"hello" == """
	hello
"""
assert to-yaml:nil == """
	null
"""
assert to-yaml:5 == """
	5
"""
assert to-yaml:[1 true "" (-1)] == """
	- 1
	- true
	- ''
	- -1
"""
assert to-yaml:{
	dict = {
		bool = true
	}
	num = 5
	ref = dict
	list = [false]
} == """
	dict:
	  bool: true
	list:
	- false
	num: 5
	ref:
	  bool: true
"""

assert type:(to-yaml:{err = error:"foo"}) == "error"
