#![no_main]

libfuzzer_sys::fuzz_target!(|source: &str| {
	let val = ecl::eval("<fuzz input>", &source);
	let _ = serde_json::to_writer(&mut std::io::sink(), &val);
});
